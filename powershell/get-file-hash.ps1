$source_dir = ""
$target_dir = ""


#$source_files = Get-ChildItem -Path $source_dir | ForEach-Object {  $_.FullName }
#$target_dir = Get-ChildItem -Path $target_dir | ForEach-Object {  $_.FullName }

cd $source_dir
$source_files = Get-ChildItem |   ForEach-Object {  $_.Name }
cd $target_dir
$target_files = Get-ChildItem   | ForEach-Object {  $_.Name }

$source_hash = @{}
$target_hash = @{}

cd $source_dir
Foreach ($file in $source_files) { 
Get-FileHash -Path $file | ForEach-Object {
    $source_hash[$file] = $_.Hash }
    #write-host "$($_.Hash) + $($_.Path)" }
}

write-host $(pwd)

cd $target_dir
Foreach ($file in $target_files) { 
#write-host $file
Get-FileHash -Path $file | ForEach-Object { 
    $target_hash[$file] = $_.Hash }
} 
#write-host $(pwd)

foreach ($h in $source_hash.GetEnumerator()) {
   # Write-Host "$($h.Value) : $($target_hash[$h.Key])"
    #Write-Host $h.Key
    if ($h.Value -eq $target_hash[$h.Name]) {
        $target_hash.Remove($h.Name)
        #write-host "Files $($h.Key) are the same.."
    }
    else {
        write-host "ok" 
        }
    #write-host "$($h.Name) : $($h.Value)"
}

write-host "===================================================" 
Write-host "Files that are not the same or only exist in target_dir:" 
foreach ($h in $target_hash.GetEnumerator()) {
    Write-Host $h.Name "--Hash-->>" $h.Value
}