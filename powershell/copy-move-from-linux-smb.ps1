$user = ""
$password = ""
$smb_server = ""
$local_drive = "K:"

$backup_folder_fio = "${local_drive}\backup\FioArtifactsQueue"
$backup_folder_dms = "${local_drive}\backup\export_1"

$source_FioArtQueue = "${local_drive}\FioArtifactsQueue" 
$target_FioArtQueue = "D:\lokaal\EFI\Processed" 
#$target_FioArtQueue = "D:\tesfio"
 
$source_ExporDmsQueue = "${local_drive}\export_1"
$target_ExporDmsQueue = "D:\lokaal\export_1"
#$target_ExporDmsQueue = "D:\testdms"

#copy the content of remote fio / dms to this server
$robo_options = "/E  /R:1 /W:1 /NP" # use /L to do a dry-run
$robo_command_fio = 'cmd.exe /C "C:\Windows\System32\Robocopy.exe ${source_FioArtQueue} ${target_FioArtQueue} ${robo_options}"'
$robo_command_dms = 'cmd.exe /C "C:\Windows\System32\Robocopy.exe ${source_ExporDmsQueue} ${target_ExporDmsQueue} ${robo_options}"'

$robo_options_backup = "/E /move /R:1 /W:1 /NP " # use /L to do a dry-run
$robo_command_fio_backup = 'cmd.exe /C "C:\Windows\System32\Robocopy.exe ${source_FioArtQueue} ${backup_folder_fio} ${robo_options}"'
$robo_command_dms_backup = 'cmd.exe /C "C:\Windows\System32\Robocopy.exe ${source_ExporDmsQueue} ${backup_folder_dms} ${robo_options}"'
#after copy is done, move the items in the fio / dms queue to the backup locations defined above.

#remove existing smb mappings
Remove-SmbMapping -LocalPath "${local_drive}" -force

#create new smb mapping
New-SmbMapping -LocalPath "${local_drive}" -RemotePath "\\$smb_server\efi_queues" -Username $user -Password $password

#check if directories contain any items to be copied
$items_in_fio = $(Get-ChildItem -Path $source_FioArtQueue).Count
$items_in_dms = $(Get-ChildItem -Path $source_ExporDmsQueue).Count

#execute robo commands for folders
#checks if items in remote directory does not equal 0, if < 0 then execute sync command and backup the content to the backup folder.
if (!($items_in_fio -eq 0)){
    Invoke-Expression -Command $robo_command_fio
    Move-Item -Path $source_FioArtQueue\* -Destination $backup_folder_fio
    }
else {
    write-host "Not moving anything from FIO art..."
    }
if (!($items_in_dms -eq 0)) {
    Invoke-Expression -Command $robo_command_dms
    Move-Item -Path $source_ExporDmsQueue\* -Destination $backup_folder_dms
    }
else {
    write-host "Not moving anything from export_1 queue"
    }

#remove smb mapping
Remove-SmbMapping -LocalPath "${local_drive}" -force