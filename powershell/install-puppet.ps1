##Installing Puppet agent on Windows
#Johnny.vanveen@kpn.com
#version 0.1
#data 08-06-2017
##Let's start!
#valid environments are production,test,acceptance,development

param(
 $environment = $null )

[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true} ; #ignoring ssl errors

$hostname = ($(hostname).ToLower()) 
$pagent_url = ".../puppet-agent-x64.msi"
$pagent_location =  "c:\puppet-agent-x64.msi"       
$allowed_environments = ("production","acceptance","test","development")
if ($environment -ne "production"){ $datacenter = "nlape1"} else { $datacenter = "nlams1"}

## variables end

##making sure args are given
if ($PSBoundParameters.Count -ne 0 -and $allowed_environments -contains $environment ){
(New-Object System.Net.WebClient).DownloadFile($pagent_url,$pagent_location) ## downloading puppet client. 

if (-not(test-path "C:\Program Files\Puppet Labs")){ # check whether install dir exists or not
write-host "Installing Puppet for: `nname: $hostname `nenvironment: $environment `ndatacenter: $datacenter"
    Invoke-Expression -Command "cmd.exe /C msiexec /qn /norestart /i $pagent_location"
    Clear-Content C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf
    Add-Content  C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf @"
[main]
server=pe00024prd.tooling.kpn.org
autoflush=true
environment =$environment
certname =$hostname

[agent]
noop=false
runinterval=1800
environment=$environment
"@
#above code configured the puppet.conf file!
#now create kpn.yaml 
Clear-Content C:\ProgramData\PuppetLabs\facter\facts.d\kpn.yaml
Add-Content C:\ProgramData\PuppetLabs\facter\facts.d\kpn.yaml @"
---
customer_name: gemeente_amsterdam
customer_environment: $environment
application_role: initial
application_name: initial
application_instance: 1
datacenter: $datacenter
greenfield: true
"@
if (-not(Select-String -Path 'C:\Windows\System32\drivers\etc\hosts' -Pattern "<ip addr of host>")){ 
    write-host "Adding host entry for puppetmaster to hosts file.."
    Add-Content C:\Windows\System32\drivers\etc\hosts "<ip>	<hostname>"}
write-host "Done." -ForegroundColor green 
}

else{ write-host "Puppet directory detected, are you sure puppet isn't installed?" -ForegroundColor Red -ErrorAction Stop} # if path exists..
}
else { write-host "Didnt receive a parameter for the environment, or a non-valid environment has been used." -ForegroundColor red}

<#
.SYNOPSIS
This is a program that downloads the puppet agent 4.X, installs and configures the puppet.conf / kpn.yaml. 
Though, you still need to add the KPN-Yaml to GitHub.

.PARAMETER environment
Valid environments are production,acceptance,test and development
.EXAMPLE
./installpuppet-agent-win01.ps1 <environment>

#>