$download_folder = [environment]::GetFolderPath("Desktop")+"\DoubleTake_8.0.0.1730.1.exe"
$double_service = Get-Service -Name "Double-Take"  -ErrorAction SilentlyContinue
$url = '...DoubleTake_8.0.0.1730.1.exe'
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true} ; #ignoring ssl errors
if ($double_service.Status -ne "Running"){
    if (-Not(Test-Path C:\Users\Administrator\Desktop\DoubleTake_8.0.0.1730.1.exe)){
       (New-Object System.Net.WebClient).DownloadFile($url,$download_folder)}
    
    New-Item c:\temp_install\doubleinstall -ItemType directory -Force     
    #Invoke-Expression -Command "cmd.exe /c wget --quiet $url -P $download_folder" }
    Invoke-Expression -Command "cmd.exe /c $download_folder /auto C:\temp_install\doubleinstall | Out-Null"
    #C:\Users\Administrator\Downloads\DoubleTake_8.0.0.1730.1.exe /auto C:\temp_install\doubleinstall | Out-Null
    Stop-Process -Name autorun
    #Copy-Item C:\temp_install\doubleinstall\setup\dt\x64\* C:\temp_install

    Clear-Content C:\temp_install\doubleinstall\setup\dt\x64\DTSetup.ini
    Add-Content C:\temp_install\doubleinstall\setup\dt\x64\DTSetup.ini @"
[Config]
DTSetupType=DTSO
WINFW_CONFIG_OPTION=ALL
LICENSE_ACTIVATION_OPTION=0
QMEMORYBUFFERMAX=1024
DISKFREESPACEMIN=250
DOUBLETAKEFOLDER="C:\Program Files\Vision Solutions\Double-Take"
DISKQUEUEFOLDER="C:\Program Files\Vision Solutions\Double-Take"
DTSERVICESTARTUP=1
DISKQUEUEMAXSIZE=UNLIMITED
PORT=6320
"@
    write-host "Starting sillent installation...."
    Invoke-Expression -Command "cmd.exe /c C:\temp_install\doubleinstall\setup\dt\x64\setup.exe /s /v'DTSETUPINI='C:\temp_install\doubleinstall\setup\dt\x64\DTSetup.ini' /qn' | Out-Null"
    Write-Host "Cleaning temp double take directory."
    Remove-Item C:\temp_install\doubleinstall -recurse
    Remove-Item $download_folder
}
else {
  write-host "Double take service is already running, installed?"
}