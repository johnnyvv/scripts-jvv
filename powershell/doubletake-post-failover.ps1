param(
 $servernaam = $null )
write-host $servernaam
Set-ExecutionPolicy RemoteSigned
Function LogWrite{
 Param ([string]$logstring)
 Add-Content $logfile -Value $logstring
}
Function CheckService{
 Param ([String]$service)
 if ((Get-Service -Name $service | select status -ErrorAction SilentlyContinue).status -eq "Running"){
   return $service}
   else { LogWrite "$service not found.." break }
}
[System.Net.ServicePointManager]::ServerCertificateValidationCallback = {$true} ; #ignoring ssl errors
$hostname = ($(hostname).ToLower()) 
$logfile = "c:\rm-tooling.log"
#$logfile = [environment]::GetFolderPath("MyDocuments")+"\rm-tooling.log"
LogWrite $(Get-Date -Format "%h:%m:%s %d/%M/%y")
$pagent_url = "http://145.222.97.151/pub/GASD/puppet-agent-x64.msi"
$pagent_location =  "c:\puppet-agent-x64.msi"             #[environment]::GetFolderPath("Desktop")+"\puppet-agent-x64.msi"
#$pagent_environment = ((Select-String -Path C:\staging-SSM\Files\ProgramData\PuppetLabs\puppet\etc\puppet.conf -Pattern environment | Select-String "environment")-split " +")[3]
$pagent_environment = ((Select-String -Path C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf -Pattern environment | Select-String "environment")-split " +")[3]
#(((Select-String -Path C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf -Pattern environment)-split ":")[3]-split "=")[1]
$services = ("GxEvMgrC(Instance001)","McShield")
$commvault = CheckService -service $services[0]
$mcafee_enterprise = CheckService -service $services[1]
$choco_uninstall =("AutoDoc", "CenterityAgent", "SnareAgent", "Sysinternals", "PuppetAgent", "simpana", "Putty", "ManagementUtils")
$mcafee_agent = "C:\Management\Programs\McAfee\Common Framework\FrmInst.exe"
$host_file = "$env:windir\System32\drivers\etc\hosts"
$yaml_location = "C:\ProgramData\PuppetLabs\facter\facts.d\kpn.yaml"
$scheduled_tasks = ("Poweb - Auto Documentation", "Update scheduled task", "CU - Overheid AutoDoc", "pe-mcollective-metadata")
$win_activated = $(Get-WmiObject -Class SoftwareLicensingProduct -Filter "PartialProductKey LIKE '%'").licensestatus
$computername = Get-WmiObject Win32_ComputerSystem
if ($pagent_environment -eq "stable"){ $pagent_environment = "production"}
write-host $pagent_environment



#edit logfile
LogWrite "Editing choco config file"
$xml_content = get-content C:\Management\Programs\Chocolatey\Config\chocolatey.config
$xml_content[0] = '<?xml version="1.0" encoding="utf-8"?>'
clear-content C:\Management\Programs\Chocolatey\Config\chocolatey.config
add-content C:\Management\Programs\Chocolatey\Config\chocolatey.config $xml_content

LogWrite "Removing Back-up route" 
Invoke-Expression -Command "cmd.exe /c route delete 145.222.45.0 -p" | Out-Null

LogWrite "Uninstalling choco packages..."
Foreach ($package in $choco_uninstall){
 try 
 {
  Invoke-Expression -Command "cmd.exe /c choco uninstall $package -f -y" | Out-Null
  #add logging..
  LogWrite "Uninstalled $package" 
  }
  catch{
  LogWrite "Error trying to uninstall $package"}
}

#Uninstalling msi way of life
$packages_msi = ("NSClient++ (x64)", "McAfee VirusScan Enterprise", "CommVault Windows File System (Instance001)", "CommVault File System Core (Instance001)")
foreach ( $msi in $packages_msi){
 $pack = Get-WmiObject -Query "SELECT * FROM Win32_Product WHERE Name = '$msi'"
 if ($pack) { 
  try{
    $pack.Uninstall() | Out-Null 
    LogWrite "Uninstalled $msi"}
   # Clear-Variable $pack}
  catch { LogWrite "Failed to uninstall $msi"}}
 else { LogWrite "$msi not found."
}}

if (Test-Path $mcafee_agent){
 #LogWrite "Uninstalling Mcafee agent.."
 Invoke-Expression -Command "cmd.exe /C '$mcafee_agent' /remove=agent /Silent"}#LogWrite "Failed to remove mcafee agent -- File $mcafee_agent not found."}

LogWrite "Downloading Puppet agent 4"
(New-Object System.Net.WebClient).DownloadFile($pagent_url,$pagent_location)
if (test-path $pagent_location){ 
  LogWrite "Installing Puppet agent 4"
  #Copy-Item C:\ProgramData\PuppetLabs -Recurse C:\ProgramData\PuppetLabs_old
  #Remove-Item -Recurse -Force C:\ProgramData\PuppetLabs 
  Stop-Service -Name pxp-agent -Force -ErrorAction SilentlyContinue
  #Stop-Process -Name pxp-agent 
  Stop-Process -Name rubyw -Force -ErrorAction SilentlyContinue
  Rename-Item C:\ProgramData\PuppetLabs "PuppetLabs_old" -Force 
  #Rename-Item C:\ProgramData\PuppetLabs\puppet\etc\ssl -NewName "ssl_old"
  #Rename-Item C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf -NewName "puppet.conf.old"
  Invoke-Expression -Command "cmd.exe /C msiexec /qn /norestart /i $pagent_location"
  Clear-Content C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf
  Add-Content  C:\ProgramData\PuppetLabs\puppet\etc\puppet.conf @"
[main]
server=pe00024prd.tooling.kpn.org
autoflush=true
environment =$pagent_environment
certname =$servernaam

[agent]
noop=false
runinterval=1800
environment=$pagent_environment
"@}
  #PUPPET_MASTER_SERVER=pe00024prd.tooling.kpn.org PUPPET_AGENT_CERTNAME=$($(hostname).ToLower())"}
else { LogWrite "Failed to install Puppet agent 4, file does not exist."}
 
#removing scheduled tasks; 
foreach ($task in $scheduled_tasks){
 try {
    Invoke-Expression -Command "cmd.exe /C schtasks.exe /delete /TN '$task' -f"
    LogWrite "Deleted scheduledtask $task"}
 catch{
  #write-host "failed $task"
    LogWrite "Could not remove scheduled task $task, does it exist?"}
}

#add initial kpn yaml 
if ($pagent_environment -eq "production"){ $datacenter = "nlams1"} else { $datacenter = "nlape1"}

Clear-Content $yaml_location -ErrorAction SilentlyContinue
Add-Content $yaml_location @"
---
customer_name: gemeente_amsterdam
customer_environment: $pagent_environment
application_role: initial
application_name: initial
application_instance: 1
datacenter: $datacenter
greenfield: true
"@

if (-not($win_activated)){
   Invoke-Expression -Command "cmd.exe /C route add 145.222.132.0 MASK 255.255.255.0 10.185.128.1"
   #Invoke-Expression -Command "cmd.exe /C slmgr /ipk 489J6-VHDMP-X63PK-3K798-CPX3Y"
   Invoke-Expression -Command "cmd.exe /C cscript //B 'C:\Windows\system32\slmgr.vbs' /ipk 489J6-VHDMP-X63PK-3K798-CPX3Y"
   Invoke-Expression -Command "cmd.exe /C cscript //B 'C:\Windows\system32\slmgr.vbs' /ato"
   #Invoke-Expression -Command "cmd.exe /C slmgr /ato"
   LogWrite "Activated Windows"
} else { LogWrite "Windows was already activated..."}

if (Test-Path C:\Windows\ccmsetup){
   Invoke-Expression -Command "cmd.exe /C C:\Windows\ccmsetup\ccmsetup.exe /uninstall" | Out-Null
   Remove-Item C:\Windows\ccmsetup -Recurse -Force
   LogWrite "Uninstalled sccm and removed the directory."
}

#adding content to /etc/hosts
Add-Content C:\Windows\System32\drivers\etc\hosts @"
#hent 

"@

#renaming computer
if ($servernaam -ne $null -and $servernaam -ne $(hostname).ToLower()){
Logwrite "Renaming $(hostname) to $servernaam"
#Rename-Computer -NewName $servernaam #only works ps 3.0+
 $computername.Rename("$servernaam")
}
else { LogWrite "could not rename computer, no pc name given or name is already what it's supposed to be!"}

#installing powershell 3.0 
LogWrite "Downloading update package"
(New-Object System.Net.WebClient).DownloadFile("http://145.222.97.151/pub/GASD/Windows6.1-KB2506143-x64.msu","c:\Windows6.1-KB2506143-x64.msu")
if (test-path c:\Windows6.1-KB2506143-x64.msu){
    LogWrite "Installing Powershell 3.0"
    Invoke-Expression -Command "cmd.exe /C wusa.exe c:\Windows6.1-KB2506143-x64.msu /quiet /norestart"
} else { LogWrite "Didn't find the msu package"}

LogWrite "Make sure that there is no certificate available for $hostname, before generating a new one!"
#LogWrite $servernaam
LogWrite "DONE.. restarting"
Restart-Computer -Force

#C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -Command "&'C:\Users\Administrator\Downloads\doubletake-concept.ps1'"