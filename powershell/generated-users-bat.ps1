#generate password file
Function writeBat{
 Param ([string]$content)
 Add-Content $output_file $content
}

$test = Import-Csv -Delimiter ";" C:\accounts.csv

$output_file = "c:\adduser$(Get-Date -Format '%d%M%y').bat"
Clear-Content $output_file
foreach ($account in $test){
writeBat @"
NET USER $($account.username) "$($account.wachtwoord)"  /comment:`"$($account.comment)`" /ADD /Y
net localgroup "$($account.group)" $($account.username)  /add
WMIC USERACCOUNT WHERE `"Name='$($account.username)'`" SET PasswordExpires=FALSE

"@
}
writeBat "net localgroup Administrators"
writeBat "timeout 5"
writeBat "del 'C:\Users\Administrator\Desktop\adduser$(Get-Date -Format '%d%M%y').bat'"