#read yaml file contents, create hash from the values!
#C:\Users\veen530\Documents\applicaties
[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
Function writeHost{
 Param ([string]$content)
 Add-Content $output_file $content
}
#$test = Import-Csv -Delimiter ";" C:\Users\veen530\Documents\applicaties\hosts.csv
#$input_file = "C:\Users\veen530\Documents\applicaties\hosts-ami.csv"
$input_file = [Microsoft.VisualBasic.Interaction]::InputBox("Enter input file path ", "Name", "c:\hosts" )
$output_file = "C:\Users\veen530\Documents\applicaties\hosts.yaml"

if (!(test-path -path $input_file -PathType Leaf)){
    write-host "File ${input_file} does not exist. Exiting..."
    exit 1
}
else {
 $input_file = Import-Csv -Delimiter ";" $input_file
}

Clear-Content $output_file
writeHost "---"
writeHost "hosts::host:"
foreach ($item in $input_file){
#write-host $item.ip
writeHost "  $($item.servernaam):"
writeHost "   ip: $($item.ip)"
writeHost "   comment: '$($item.comment)'"
if ([string]::IsNullOrEmpty($($item.alias))) { 
    continue
} else {    
    writeHost "   host_aliases:"
    $aliases = $item.alias.Split(' ')
    foreach ($alias in $aliases){
      writeHost "    - '$($alias)'"
    }
}
}
