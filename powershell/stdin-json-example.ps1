$result = @{}
$exitcode = 1

foreach ($i in $input){
    if ($i -like '*interface*'){
      $interface = ConvertFrom-Json $i
    }
    else{
      write-host "sorry"
      $result['error']=@{'msg'='interface key not found.'}
    }
   }

write-host $interface."interface"

#Disable the Adapter
if ([string]::IsNullOrEmpty($interface."interface") -or (-not (Get-NetAdapter -name $interface."interface" -ErrorAction SilentlyContinue))) {
  $result['error']=@{'msg'="Interface cannot be empty or interface does not exist."}
}
else {
  Disable-NetAdapter -Name $interface."interface" -Confirm:$False
  $result['changed']=$True
  $exitcode = 0
}

ConvertTo-Json $result
exit $exitcode