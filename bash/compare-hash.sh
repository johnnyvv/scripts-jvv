#!/bin/bash

source_dir="" #directory of old files
target_dir="" # directory of the new files
echo "changing into directory $target_dir"
outfile="/tmp/hashcompare.txt"
search_pattern="ls *.php"

cd $target_dir
pwd
echo "Listing all files in $target_dir with sha1 hash"
declare -a tgr_files=($($search_pattern))

cd $source_dir
pwd
declare -a src_files=($($search_pattern))

for srcfile in ${src_files[*]}; do
  for trgfile in ${tgr_files[*]}; do
    if [[ "$srcfile" == "$trgfile" ]] ; then
      echo "Found duplicate"
      echo "Checking hashes...."
      srchash=$(openssl dgst -sha256 "$source_dir/$srcfile" | awk -F '= ' '{print $2}')
      trghash=$(openssl dgst -sha256 "$target_dir/$trgfile" | awk -F '= ' '{print $2}') # can also use sha256sum instead of openssl.
      if [[ "$srchash" == "$trghash" ]] ; then
        echo "Found matching hashes:" >> $outfile
        echo "Hash of source $srcfile -> $srchash" >> $outfile
        echo "Hash of target $trgfile -> $trghash" >> $outfile
     else
        echo "Hash doesn't match"
        echo "Hashes don't match:" >> $outfile
        echo "Hash of source $srcfile -> $srchash" >> $outfile
        echo "Hash of target $trgfile -> $trghash" >> $outfile
     fi
    else
      continue
    fi
  done
done