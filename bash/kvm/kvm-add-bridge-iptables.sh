#!/bin/env bash

#check if script is run as root (0)
if [[ "$EUID" -ne 0 ]]; then
  echo "This script must be run as root user."
  exit 
fi

bridge_inf="jvv-br0"
dummy_inf="jvv-dummy"
bridge_network="10.0.0.0"
bridge_mask="24"

gate=$(($(echo $bridge_network | awk -F '.' '{print $4}')+1))

#increase the last octedo
gateway=$(echo $bridge_network | awk -F '.' '{print $1"."$2"."$3}').${gate}

#create the interfaces
#load module
if lsmod | grep 'dummy' > /dev/null; then
  echo "Module loaded"
else
  echo "Module not loaded"
  echo "Loading dummy module..."
  modprobe -a dummy
fi

#add necessary interfaces
ip link add name ${dummy_inf} type dummy
ip link add name ${bridge_inf} type bridge
ip link set ${dummy_inf} up
ip link set ${bridge_inf} up
#add bridge to dummy inf
ip link set dev ${dummy_inf} master ${bridge_inf}
ip addr add ${gateway}/${bridge_mask} dev ${bridge_inf}

#Do not forward reserved addresses / the broadcast address 
iptables -t nat -A POSTROUTING -s ${bridge_network}/${bridge_mask} -d 224.0.0.0/24 -j RETURN
iptables -t nat -A POSTROUTING -s ${bridge_network}/${bridge_mask} -d 255.255.255.255/32 -j RETURN

#Allow traffic from bridge network to external networks
#allow tcp
iptables -t nat -A POSTROUTING -s ${bridge_network}/${bridge_mask} ! -d ${bridge_network}/${bridge_mask} -p tcp -j MASQUERADE --to-ports 1024-65535
#allow udp 
iptables -t nat -A POSTROUTING -s ${bridge_network}/${bridge_mask} ! -d ${bridge_network}/${bridge_mask} -p udp -j MASQUERADE --to-ports 1024-65535
#allow other protocols 
iptables -t nat -A POSTROUTING -s ${bridge_network}/${bridge_mask} ! -d ${bridge_network}/${bridge_mask} -j MASQUERADE

#Accept to forward these packets, these rules are only important when the default policy on the forward change is block
#Incoming packets
iptables -t filter -A FORWARD -d ${bridge_network}/${bridge_mask} -o ${bridge_inf} -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
#Outgoing packets
iptables -t filter -A FORWARD -s ${bridge_network}/${bridge_mask} -i ${bridge_inf} -j ACCEPT
#traffic between VMS
iptables -t filter -A FORWARD -i ${bridge_inf} -o ${dummy_inf} -j ACCEPT

#enable ip forward in kernel

if [[ ! $(echo `sysctl net.ipv4.ip_forward -n`) = 1 ]]; then
  echo "Forwarding not enabled"
  echo "Enabling forwarding..."
  sysctl net.ipv4.ip_forward=1 > /dev/null
fi