#!/bin/env bash
process="cvlaunchd"
processes=$(ps -ef | grep ${process}  | grep -v 'grep' | wc -l)
temp_file="/opt/commvault/simpana/Base/Temp"

if [[ "$processes" -eq 0 ]]; then
	echo "zero processes, stopping and starting simpana"
	systemctl stop Galaxy
	if [[ -f "$temp_file" ]]; then
		echo "Removing directory ${temp_file}..."
		rm -f $temp_file
	fi
	systemctl start Galaxy
elif [[ "$processes" -eq 2 ]]; then
	echo "2 processes"
	echo "Stopping Galaxy"
	systemctl stop Galaxy 
	echo "Checking if all ${process} processes are gone, if not killing them with force(-9)"
	ps -ef | grep cvlaunchd  | grep -v 'grep' | awk '{print $2}' | xargs kill -9
	echo "Starting Galaxy"
	systemctl start Galaxy
	echo "Is Galaxy running: $(systemctl is-active Galaxy)"
else
	echo "not performing any action..."
fi