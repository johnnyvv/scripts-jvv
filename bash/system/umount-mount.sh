#!/bin/env bash
#mount
mount=$1
sleepTime=30
logfile="/root/$(echo $mount | xargs -i python -c 'print "{}".split("/")[-1]').log"
logfileSize=$(stat --format=%s $logfile 2> /dev/null) #size is in bytes
maxLogfileSize=2000000 #setting max size 2MB, size in byte(s)

if [[ $logfileSize -gt $maxLogfileSize ]] ; then
  #truncate the file
  truncate -s 0 $logfile
fi

writeToLog() {
  echo "${1}" >> $logfile
}

mountStatus() {
  #dont need any parameter, as it will check the mount define in variable 'mount'
  if [[ "$(findmnt -n ${mount})" ]] ; then
    if [[ $(lsof -t +f -- $mount | wc -l) -eq 0 ]]; then
       return 0 #return exit code 0, meaning succes
    fi
   return 1 #1 here is no boolean, it's the exit code number that is sent back.
  else
   return 1
  fi
}

umountAndMountTarget(){
if mountStatus ; then
  writeToLog "Mount ${mount} is ready for umounting.."
  echo "Umounting ${mount}"
  if ! $(umount -v $mount >> $logfile 2>&1); then
    writeToLog "Failed to umount, will try to do an lazy umount.."
    umount -v -l $mount  >> $logfile 2>&1
  fi
  echo "Sleeping for ${sleepTime} second(s)" >> $logfile
  sleep $sleepTime
  mount -v $mount >> $logfile 2>&1
else
  if [[ ! "$(findmnt -n ${mount})" ]] ; then
    writeToLog "${mount} not mounted."
  #check by which pid / user / cmd they are in use..
  else
    writeToLog "Unable to umount ${mount} as it is in use by:"
    pids=$(lsof -t +f -- $mount)
    for pid in ${pids[*]}; do
      writeToLog "PID ${pid}: $(ps --no-headers -o user,cmd,state -p ${pid})"
    done
  fi
fi
}

writeToLog "##### $(date) #####"
umountAndMountTarget