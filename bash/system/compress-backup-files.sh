#!/bin/env bash

if [[ $EUID -eq 0 ]]; then
   echo "Don't run this script as root user"
   exit 1
fi

queue_root="" #starting point of this script
#queue_root="/apps/oracle_acc/efi/queues/acc"
logfile="${queue_root}/backup/archive-job.log"
files_date_to_copy=0 # value is 0 since we want to create a archive everyday with the files of that day, might need to check the date
delete_archive_after=84 #84 weeks the logs need to be kept. after 84 days that archive can be deleted.
declare -a dirs_to_backup=("MessageStatusQueue" "ImportArchive") #which queue directory to move to the backup folder
#declare -a dirs_to_compress=("FioArtifactsQueue" "export_1" "ImportArchive" "MessageStatusQueue" "SystemDataQueue") # which directories need to be compressed within backup dir
declare -a dirs_to_compress=("export_1" "MessageStatusQueue" "ImportArchive" "FioArtifactsQueue" "SystemDataQueue") # which directories need to be compressed within backup dir

if [[ -z "${queue_root}" ]]; then
  echo "Cannot run script without a queue_root."
  exit 1
fi

function log_write {
echo "$(date +'%d-%m-%Y-%H:%M:%S') - $1" >> $logfile
}

log_write "#### Script started by ${USER} #####"

#first copy data to backup folder
cd $queue_root
for dir_to_backup in ${dirs_to_backup[*]}; do
  if [[ -d $dir_to_backup ]] && [[ -d backup/$dir_to_backup ]] ; then
    items_dir=$(find $dir_to_backup -not -path $dir_to_backup | wc -l)
    #check if dir is empty
    if [[ "${items_dir}" -gt 0 ]] ; then
      log_write "Moving ${items_dir} item(s) from ${dir_to_backup} to backup folder"
    #we move everything from today, to the backup location
    #another script will zip the items
    #find ImportArchive -ctime +0 | xargs -i echo {}
    #find $dir_to_backup -ctime +$files_date_to_copy #| xargs -i mv {} backup/$dir_to_backup/
    #find $dir_to_backup -not -path $dir_to_backup | xargs -i mv {} backup/$dir_to_backup/
      find $dir_to_backup -not -path $dir_to_backup | xargs -i mv {} backup/$dir_to_backup/
    else
      log_write "No file(s) in ${dir_to_backup} to move"
    fi
  else
    log_write "Directory structure for ${dir_to_backup} not set"
  fi
done

#now we compress all the fiiles in the backup folder with the date of yesterday/today
unset $dir_to_backup

cd $queue_root
archive_type="zip"
for dir_to_backup in ${dirs_to_compress[*]}; do
  if [[ -d $queue_root/backup/$dir_to_backup ]] ; then
    cd $queue_root/backup
    # we set max-depth 1 since 7za will auto add the files within those directories in the archive
    #the number of files found...
    #find_found=$(find $dir_to_backup -not -name *.7z -mtime +$ | wc -l) #might add a check to see if this is 0 that we break out
    find_found=$(find $dir_to_backup -not -path $dir_to_backup -not -name *.$archive_type | wc -l) #might add a check to see if this is 0 that we break out
    echo "Found: ${find_found} file(s) in directory ${dir_to_backup}."
    #compress and delete source files by using the 7za swtich -sdel, if you wish to keep the files simple remove the switch..
    if [[ "${find_found}" -gt 0 ]] ; then
      cd $dir_to_backup
      archive_name="${dir_to_backup}-backup-$(date +'%d-%m-%Y').${archive_type}"
      log_write "Backing up ${find_found} file(s) in ${dir_to_backup} - to file ${archive_name}"
       #cd $dir_to_backup
      7za -t$archive_type -sdel a $archive_name * -x\!*.$archive_type
    else
      log_write "Did not perform any action as no files are found to backup in ${dir_to_backup}"
    fi
  else
    log_write "Failed to create archive for ${dir_to_backup} as the folder backup does not exist."
  fi
done