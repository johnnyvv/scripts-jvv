#!/bin/env bash

#to do:
#might add a main vg so that main vg wont be touched! since you will most likely run it from a live cd or w/e..

#command to corrupt xfs system: xfs_db -x -c blockget -c "blocktrash -s 512109 -n 1000" /dev/vg01/lv02
#get the volume groups
declare -a vgs_server
declare -A lvs_dict
declare -a success
declare -a failed

vgs_server=$(vgs --rows --no-headings | head -n 1)
mount_point="/mnt"
logfile="/tmp/xfs-repair.log"

function umount_partition {
  if umount $1 2> /dev/null; then
    echo "succesfully unmounted ${1}"
    return 0
  else
    echo "Failed to unmount ${1}"
    return 1
  fi
}

function mount_partition {
  if ! findmnt $1; then
    echo "${1} not mounted, mounting.."
    mount -v $1
  else
    echo "${1} mounted.. skipping"
  fi
}

function check_not_mounted {
#check if path / device is mounted or not
if ! findmnt $1 > /dev/null 2>&1; then
  return 0 #return ok since we only run when fs is not mounted
else
  return 1 #return NOK since we cant do repair on a mounted fs
fi
}

function repair_xfs {
#this function repairs the xfs parition by first mounting / umounting the partition. It will not destroy the LOG!
#to add loop count.. we dont want the xfs to keep repairing for unlimted # times.
#first check if repair is needed..
if xfs_repair -n $1 >> logfile 2>&1; then
  echo "Repair not needed for lv:${1}"
else
  #still need to check if all goes well, since when you do xfs_db -x -c 'logformat' you cant use the disk but xfs says it's ok
  echo "Repair needed for lv:${1}"
  echo "Repairing....check logfile: ${logfile} for details.."
  xfs_repair $1 >> $logfile 2>&1
  echo "done, checking partition again"
  repair_xfs $1
fi
}

for vg in $vgs_server; do
  for lv in $(ls /dev/$vg/);do
    if [[ $(echo $(udevadm info --query=property /dev/$vg/$lv | grep ID_FS_TYPE | awk -F = '{print $2}')) = "xfs" ]]; then
      if check_not_mounted /dev/$vg/$lv;then
        echo "${lv} not mounted"
        #now mount the xfs partition to load the logs
        #mount x on $mount_point
        #umount the partition, check for errors
        repair_xfs /dev/$vg/$lv
      else
        echo "${lv} is mounted"
        if umount_partition /dev/$vg/$lv; then
          #repair
          repair_xfs /dev/$vg/$lv
        fi
      fi
    else
      echo "Skipping ${lv} as type does not equal xfs"
      #echo "${lv} is not xfs}"
    fi
  done
done