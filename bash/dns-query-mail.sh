#!/bin/env bash
ipaddr="216.58.212.131"
host="google.nl"
dns_server="<ip of the name server you wish to talk to.."

while [[ $(dig @$dns_server ${host} +short) =  "${ipaddr}" ]]
do
  echo "Still old addres.."
  sleep 60
done
mail -s 'ip address changed!' '<your email address' <<EOF
ip address of host ${host} changed to: $(dig @$dns_server ${host} +short)
EOF