#!/bin/env bash

alias puppet="/opt/puppetlabs/bin/puppet"

#simpana_status=$(/opt/puppetlabs/bin/facter -p simpana)
simpana_status=true
simpana_folder="/opt/commvault"

puppet_class_file="/opt/puppetlabs/puppet/cache/state/classes.txt"

puppet_noop=$(puppet config --section agent print noop)

declare -a cron_allowed=($(cat /etc/cron.allow | grep -v '^#'))
declare -a classes_brownfield=("profile_linux::mcafee" "profile_linux::simpana_agent" "profile_linux::satellite" "profile_linux::easy2audit" "profile_linux::centerity_agent" "profile_linux::ldap" "profile_linux::network::routing")
declare -a classes_missing

for class in ${classes_brownfield[*]} ; do
	if [[ $(grep ^${class} ${puppet_class_file}) ]] ; then
		echo "found class ${class}"
	else 
		classes_missing+=("$class")
    fi
done

if [[ "${#classes_missing[@]}" -ne 0 ]] ; then
  echo "The following classes are missing:"
  for class in ${classes_missing[*]} ; do
    echo "$class"
  done
fi

#Create a function to start a puppet run with tags a
function runPuppet(){
	tag=$1
	if [[ "$puppet_noop" ]] ; then
		puppet agent -t --tags=$tag --no-noop
	else
		puppet agent -t --tags=$tag
    fi
}

function continueRun() {
    read -p "Do you wish to continue? press y for Yes: " go
    if [[ "$go" = "y" ]] ; then
        true
    else
        echo "Exiting..."
        exit 1
    fi
}

#class_mcafee=$(if [[ $(grep ^mcafee::linux_agent ${puppet_class_file}) ]];then echo "true"; else echo "false";fi)

#check if simpana is installed
if [[ "$simpana_status" && -d "$simpana_folder" ]] ; then
	echo "Simpana is already installed - No need to run Puppet for now. Running with tags for simpana agent"
else
    echo "Running Puppet with tags for simpana agent"
	runPuppet "profile_linux_simpana_agent"
fi

#Installing easy2audit module
echo "Content of /etc/cron.allow"
for i in ${cron_allowed[*]} ;do
  echo "User: $i"
done
echo "Puppet will change the content of the file to root + the names specified in the server yaml. Make sure you add the other usernames to the yaml before continueing"
continueRun
#removing ipa-client, else the run will fail

if [[ $(yum list installed ipa-client -C) ]] ; then
    yum remove ipa-client -C -y
runPuppet "profile_linux_easy2audit"