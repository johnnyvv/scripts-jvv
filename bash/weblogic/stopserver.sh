#!/bin/bash
action=$1
alias date='date +"%Y-%M-%d-%H:%M:%S"'
adminserver=AdminServer
domain=E1_Apps
listen_interface="eth1"
domain_home="/apps/oracle/Middleware/Oracle_Home/user_projects/domains/$domain"
mslistenaddr=$(/sbin/ip a | grep $listen_interface | grep inet | awk '{print $2}' | awk -F '/' '{print $1'})
logfiledir="/apps/oracle/Middleware/Oracle_Home/user_projects/domains/$domain"
logfile="/tmp/${domain}-stop.log"
logfileout="/tmp/${domain}_${adminserver}.out"
declare -a msservers=("MS1" "MS2")
declare -A msports
declare -A mspids
msports=( ["MS1"]=7005 ["MS2"]=7007 )
admlistenaddr=$(/sbin/ip a | grep $listen_interface | grep inet | awk '{print $2}' | awk -F '/' '{print $1'})
admlistenport=7001
admlistensocket=${admlistenaddr}:${admlistenport}
adminurl="t3://$admlistensocket"

function checkAdminState
{
admsocketcheck=$(netstat -tulpn 2> /dev/null | grep $admlistensocket | awk '{print $4}')
if [[ "$admsocketcheck" == "$admlistensocket" ]] ; then
  echo "running"
else
  echo "stopped"
fi
}

function checkManagedState
{
msserver=$1
mssocketcheck=$(netstat -tulpn 2> /dev/null | grep $mslistenaddr:${msports[$msserver]}| awk '{print $4}')
if [[ "$mssocketcheck" == "$mslistenaddr:${msports[$msserver]}" ]] ; then
  #echo "$msserver -> socket $mssocketcheck."
  echo "running"
else
  echo "stopped"
fi
}

function checkManagedServersState
{
for msserver in ${msservers[*]} ; do
  mssocketcheck=$(netstat -tulpn 2> /dev/null | grep $mslistenaddr:${msports[$msserver]}| awk '{print $4}')
  if [[ "$mssocketcheck" == "$mslistenaddr:${msports[$msserver]}" ]] ; then
  echo "Managed server $msserver is running."
  else
  echo "Managed server $msserver is currently down."
  fi
done
}
function stopAdminServer
{
admstate=$(checkAdminState)
if [[ "$admstate" == "running" ]] ; then
  echo "Admin server is already stopped. Skipping"
  i=0
  while [[ "$admstate" == "running" && "$i" -lt 10 ]] ; do
    if [[ "$i" -eq 0 && -f "$domain_home/servers/$adminserver/security/boot.properties" ]] ; then
      admstate=$(checkAdminState)
      echo "$(date) Stopping $adminserver. Attempt:$i" >> $logfile
      i=$(($i+1))
      cd "$domain_home/bin"
    #nohup sh stopManagedWebLogic.sh $msserver $adminurl >
    #  nohup sh stopWebLogic.sh >> admlogfileout 2>&1 &
      echo "$(date) Admin server must be stopped using systemctl." >> $logfile
      echo "Stopped $adminserver."
      sleep 20
    else
      admstate=$(checkAdminState)
      echo "$(date) Waiting for $adminserver to be shutdown. Attempt:$i - Waiting for $(($i*20)) seconds." >> $logfile
      i=$((i+1))
      sleep 20
    fi
  done
else
  echo "Admin server is already stopped. Skipping" >> $logfile
  echo "Admin server is already stopped. Skipping"
fi
}

function stopManagedServers
{
for msserver in ${msservers[*]} ; do
  i=0
  msstate=$(checkManagedState $msserver)
  if [[ "$msstate" == "running" ]] ; then
    mspid=$(netstat -tulpn 2> /dev/null | grep $mslistenaddr:${msports[$msserver]} | awk '{print $7}' | awk -F "/" '{print $1}')
    echo $mspid
    #append pids to dictionary
    mspids+=(["$msserver"]="$mspid")
    if [[ ! -z "$mspid" && -f "$domain_home/servers/$msserver/security/boot.properties" ]] ; then
      echo "Here I am $msserver"
      echo "$(date) Stopping $msserver with PID $mspid." >> $logfile
      cd "$domain_home/bin"
      nohup sh stopManagedWebLogic.sh $msserver $adminurl >> "${logfiledir}/servers/${msserver}/logs/${msserver}.out" 2>&1 &
      echo "$(date) Executed stopManagedWeblogic script for $msserver." >> $logfile
    fi
  fi
done
sleep 60 #waiting 1 minute before shutting down with force"
for oldmspid in "${!mspids[@]}" ; do
  echo "$oldmspid has pid ${mspids[$oldmspid]}"
  mspid=$(ps -p $oldmspid 2> /dev/null)
  if [[ ! -z "$mspid" ]] ; then
    echo "$(date) Shutting down $oldmspid with force.."
    kill -9 $mspid >> $logfile 2>&1  
    echo "$(date) Shutted down $oldmspid with force.."
  else
    echo "($date) $oldmspid has been shutdown, no need for force."
  fi
done
}
#new log file entry:
echo "##################################" >> $logfile
echo "####### $(date) #######" >> $logfile

case $action in
  stopall)
    stopManagedServers
  ;;
  stopadm)
    stopAdminServer
  ;;
  stopms)
    stopManagedServers
  ;;
  statusms)
    checkManagedServersState
  ;;
  *)
    echo "Usage: $0 {stopall | stopadm | stopms | statusms }"
    echo "No parameter given." >> $logfile
esac