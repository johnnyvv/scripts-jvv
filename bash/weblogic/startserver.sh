#!/bin/bash
action=$1
alias date='date +"%Y-%M-%d-%H:%M:%S"'
adminserver=AdminServer
listen_interface="eth1"
domain=E1_Apps
domain_home="<path>/$domain"
declare -a msservers=()
declare -A msports
msports=( ["OPL920_SU444V1122"]=7004 ["AC920_SU444V1122"]=7005 )
mslistenaddr=$(/sbin/ip a | grep $listen_interface | grep inet | awk '{print $2}' | awk -F '/' '{print $1'})

logfile="/tmp/${domain}-start.log"
logfileout="/tmp/${domain}_${adminserver}.out"

admlistenaddr=$(/sbin/ip a | grep $listen_interface | grep inet | awk '{print $2}' | awk -F '/' '{print $1'})
admlistenport=7001
admlistensocket=${admlistenaddr}:${admlistenport}
adminurl="t3://$admlistensocket"
setdomain_file="$domain_home/bin/setDomainEnv.sh"
#. $setdomain_file
USER_MEM_ARGS="-Xms3072m -Xmx3072m"
export USER_MEM_ARGS

function checkAdminState
{
admsocketcheck=$(netstat -tulpn 2> /dev/null | grep $admlistensocket | awk '{print $4}')
if [[ "$admsocketcheck" == "$admlistensocket" ]] ; then
  echo "running"
else
  echo "stopped"
fi
}

function checkManagedState
{
msserver=$1
mssocketcheck=$(netstat -tulpn 2> /dev/null | grep $mslistenaddr:${msports[$msserver]}| awk '{print $4}')
echo "$mssocketcheck"
if [[ "$mssocketcheck" == "$mslistenaddr:${msports[$msserver]}" ]] ; then
  #echo "$msserver -> socket $mssocketcheck."
  echo "running"
else
  echo "stopped"
fi
}

function checkManagedServersState
{
for msserver in ${msservers[*]} ; do
  mssocketcheck=$(netstat -tulpn 2> /dev/null | grep $mslistenaddr:${msports[$msserver]}| awk '{print $4}')
  if [[ "$mssocketcheck" == "$mslistenaddr:${msports[$msserver]}" ]] ; then
  echo "$(date) Managed server $msserver is running." >> $logfile
  echo "Managed server $msserver is running."
  else
  echo "Managed server $msserver is currently down."
  echo "$(date) Managed server $mmserver is currently down." >> $logfile
  fi
done
}

function startAdminServer
{
admstate=$(checkAdminState)
if [[ "$admstate" == "running" ]] ; then
  echo "$(date) Admin server is already running." >> $logfile
  echo "Admin server is already running. Skipping"
else
  if [[ -f "$domain_home/servers/$adminserver/security/boot.properties" ]] ; then
    echo "$(date) Starting admin server" >> $logfile
    cd "$domain_home/bin"
    pwd
    #nohup sh startWebLogic.sh >> ${domain_home}/servers/${adminserver}/logs/${adminserver}.out 2>&1 &
    sleep 40
  else
    echo "$(date) No boot.properties file has been found, exiting" >> $logfile
  fi
fi
}

function startManagedServers
{
sleep 60 #waiting a minute before starting ms servers
admstate=$(checkAdminState)
if [[ "$admstate" == "running" ]] ; then
  for msserver in ${msservers[*]} ; do
    if [[ -f "$domain_home/servers/$msserver/security/boot.properties" && $(echo $(checkManagedState $msserver)) == "stopped" ]] ; then
      echo "$(date) Starting $msserver" >> $logfile
      cd "$domain_home/bin"
      #echo "nohup sh startManagedWebLogic.sh $msserver $adminurl >> ${domain_home}/servers/${msserver}/logs/${msserver}.out 2>&1 &"
      nohup sh startManagedWebLogic.sh $msserver $adminurl >> ${domain_home}/servers/${msserver}/logs/${msserver}.out 2>&1 &
      echo "$(date) Started $msserver" >> $logfile
    else
      echo $(checkManagedState $msserver)
      echo "$domain_home/servers/$msserver/security/boot.properties"
      echo "$(date) No boot.properties file has been found for $msserver, or $msserver is already running." >> $logfile
      continue
    fi
  done
else
  echo "$(date) Admin server not yet up, please start and run this scripts again." >> $logfile
  echo "Admin server not yet up."
fi
}
#new log file entry:
echo "####### $(date) #######" >> $logfile
#main program
case $action in
  startall)
    startManagedServers
  ;;
  startadm)
    startAdminServer
  ;;
  startms)
    startManagedServers
  ;;
  statusms)
    echo $USER_MEM_ARGS
    checkManagedServersState
  ;;
  x)
    checkAdminState
  ;;
  *)
    echo "Usage: $0 {startall | startadm | startms  | statusms }"
    echo "No parameter given." >> $logfile
esac