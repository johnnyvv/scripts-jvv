from sqlalchemy import Table, Column,Integer,String
from sqlalchemy.orm import mapper
from databaseBase import metadata, db_session

class User(object):
    def __init__(self, name=None,email=None):
        self.name = name
        self.email = email
    
    def __repr__(self):
        return "<User {} email {}".format(self.name,self.email)

users = Table('users', metadata,
    Column('id',Integer,primary_key=True),
    Column('name',String(50),unique=True, nullable=False),
    Column('email',String(120),unique=True, nullable=False)
)
mapper(User, users)
users.select.execute()