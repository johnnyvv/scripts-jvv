from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app import app
# pylint: disable=no-member

db = SQLAlchemy(app)
"""
In your app config, add the following config:
    SQLALCHEMY_DATABASE_URI ="sqlite:///test.db"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
"""

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)

    def __init__(self, username=None,email=None):
        self.username = username
        self.email = email

    def __repr__(self):
        return '<User {} and {}'.format(self.username,self.email)

db.create_all() # create database schema
""" db.create_all()
admin = User(username='admin', email='admin@example.com')
user1 = User(username="user1", email="bassie@bas.loc")
#db.session.add(admin)
#db.session.add(user1)
#db.session.commit()
one = User.query.filter_by(username="user1").first()
print(one.username) # to print a column it must match with result.<column_name>
#print(User.query.all()) """

#With constructor
#Create new row
new_ex = User("johnny","johnny@jvv.loc")
db.session.add(new_ex)
db.session.commit()
#select something
result = User.query.filter_by(username="johnny").first()
#print(result.username)
#to update simply
result.username = "New name"
result.email = "new@email.we"
db.session.commit()
print(User.query.all())

#to delete:
#lets first add another user to database
db.session.add(User("Henki","tank@my.tk"))
db.session.commit()
print(User.query.all())
a = User.query.filter_by(username="Henki").first()
print(a.username)
#to actually delete, simply:
db.session.delete(User.query.filter_by(username="Henki").first())
#instead of passing the User.query... you can also pass in the object which is made from the User.query.. like var a and result:
#db.session.delete(a)
db.session.commit()
print(User.query.all())