import os
basedir = os.path.abspath(os.path.dirname(__name__))

class Config(object):
    DEBUG = True
    TESTING = False
    SECRET_KEY = "Never guess" #The SECRET_KEY is needed to keep the client-side sessions secure. Choose that key wisely and as hard to guess and complex as possible.
    DATABASE = os.path.join(basedir,"flaskr.db")
    CONFIG_FILE = os.path.join(basedir, "config.py")
    USERNAME = "admin"
    PASSWORD = "default"
    APP_NAME = "FLASKR"
    BASE = basedir
    #SQLALCHEMY_DATABASE_URI ="sqlite:///test.db"
    SQLALCHEMY_DATABASE_URI ="sqlite://" # in memory database
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = True
