from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from .flaskr import app

db = SQLAlchemy(app)

#from models import entries
