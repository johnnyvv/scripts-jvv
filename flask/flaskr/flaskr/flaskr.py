#http://flask.pocoo.org/docs/0.12/tutorial
#https://www.artima.com/weblogs/viewpost.jsp?thread=240808

import os
import sqlite3
from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig') # can also define options by passing a dict to app or load it from a file / env variables e.g. app.config.from_pyfile('application.cfg', silent=True)
os.environ["FLASKR_SETTINGS"] = app.config["CONFIG_FILE"]
app.config.from_envvar('FLASKR_SETTINGS',silent=True)
print(os.environ["FLASKR_SETTINGS"])
#Simply define the environment variable FLASKR_SETTINGS that points to a config file to be loaded. The silent switch just tells Flask to not complain if no such environment key is set.


def connect_db():
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    """Opens a new database connection if there is none yet for the
    current application context.
    """
    if not hasattr(g,'sqlite_db'):
        g.sqlite_db = connect_db()
    return g.sqlite_db

@app.teardown_appcontext #http://flask.pocoo.org/docs/0.12/appcontext/#app-context
def close_db(error):
    """Closes the database again at the end of the request """
    if hasattr(g,'sqlite_db'):
        g.sqlite_db.close()

def init_db():
    if os.path.isfile(app.config["DATABASE"]):
        print("Please delete file {} if you wish to reinitialize the database.".format(app.config["DATABASE"]))
        return False
    else:
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
        return True

@app.cli.command('initdb')
def initdb_command():
    """initializes the database."""
    result = init_db()
    print(result)
    if not result:
        print("Did not initialize the database, as it already existed.")
    else:
        print("Initialized the database.")

from .routes import app

#to run
"""
export FLASK_APP=flaskr
export FLASK_DEBUG=true
flask run # be in the root of your project so flaskr/.....

#to create db:
sudo pip3 install --editable .
flask initdb
"""

#print(app.root_path)