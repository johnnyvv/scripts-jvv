from flask_sqlalchemy import SQLAlchemy
from .flaskrdb import db
# pylint: disable=no-member 
class Entries(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text, nullable=False)
    text = db.Column(db.Text,nullable=False)

    def __init__(self, title=None,text=None):
        self.title = title
        self.text = text

    def __repr__(self):
        return '<User {} and {}'.format(self.title,self.text)

db.create_all()