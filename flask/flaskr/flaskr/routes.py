from .flaskr import app,get_db
from flask import session,abort,flash, url_for, redirect, request, render_template
from .models import db
from .models import Entries
# pylint: disable=no-member
@app.route('/')
def show_entries():
    #db = get_db()
    cur = Entries.query.all()
    #cur = db.execute("select title,text from entries order by id desc")
    print(type(cur))
    #entries = cur.fetchall()
    print(cur)
    return render_template('show_entries.html', entries=cur)

@app.route('/add', methods=['POST'])
def add_entry():
    if not session.get('logged_in'):
        abort(401)
    add_row = Entries(title=request.form['title'],text=request.form['text'])
    db.session.add(add_row)
    db.session.commit()

    #db = get_db()
    #db.execute("insert into entries (title, text) values (?,?)",[request.form['title'],request.form['text']])
    #db.commit()
    flash('New entry was successfully posted')
    return redirect(url_for('show_entries'))

@app.route('/login', methods=['GET','POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_entries'))
    return render_template('login.html',error=error)

@app.route('/logout')
def logout():
    print(session)
    session.pop('logged_in', None)
    flash('You were loggged out')
    return redirect(url_for('login'))
