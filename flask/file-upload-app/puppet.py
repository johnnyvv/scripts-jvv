import json
import os
import datetime

class PuppetCatalogReader(object):

	def __init__(self,catalog="/tmp/upload/a.json"):
		if os.path.isfile(catalog):
			self._catalog= json.load(open(catalog))
			#print(self._catalog)
		else:
			print("Couldnt find catalog file {}".format(catalog))


	#getter / setter
	@property
	def catalog(self):
		return self._catalog
	
	@catalog.setter
	def catalog(self, x):
		if os.path.isfile(x):
			self._catalog = json.load(open(x))

	def printCat(self):
		print(self._catalog)
		
	def get_catalog_server(self):
		return self._catalog['name']

	def get_subjects(self):
		a = []
		for subject in self._catalog:
			a.append(subject)
		return a

	def get_catalog_date(self):
		return datetime.datetime.fromtimestamp(self._catalog['version']).strftime('%c')

	def get_catalog_env(self):
		return self._catalog['environment']

	def get_number_resources(self):
		return len(self._catalog['resources'])

	def get_resource_types(self):
		arr = []
		for resource in self._catalog['resources']:
			if resource['type'] in arr:
				pass
			else:
				arr.append(resource['type'])
		return arr

	def get_parameters_resource(self):
		for resource in self._catalog['resources']:
			#print(type(resource))
			if "parameters"  in resource:
				print(resource['parameters'])
				print("=============================================")
			else:
				continue

	def print_resource_skel(self):
		for resource in self._catalog['resources']:
			#print(resource)
			#break
			PuppetCatalogReader.print_dict_loop(resource)
			break
	def number_of_resource(self):
		d = {}
		for resource in self._catalog['resources']:
			if resource['type'] in d:
				d[resource['type']] = d[resource['type']] + 1
			else:
				d[resource['type']] = 1
		if not len(d) == 0:
			return d

	def catalog_summary(self):
		summary_dict = {}
		summary_dict['server'] = self.get_catalog_server()
		summary_dict['compiled_on'] = self.get_catalog_date()
		summary_dict['num_resources'] = self.get_number_resources()
		number_resource = self.number_of_resource()
	
		#summary_dict.update(number_resource)
		#return summary_dict
		return [summary_dict,number_resource]
		#print(summary_dict)
		#PuppetCatalogReader.print_dict_loop(summary_dict)
		#PuppetCatalogReader.print_dict_loop(self.number_of_resource())

#static methods

	@staticmethod
	def print_arr_loop(arr):
		for index in arr:
			print(index)
	@staticmethod
	def print_dict_loop(dict_in):
		for key,value in dict_in.items():
			print("{} ==> {}".format(key,value))