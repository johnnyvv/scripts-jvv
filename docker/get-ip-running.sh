#!/usr/bin/env bash
echo $0
declare -a myarr=$(docker ps | awk '$1 != "CONTAINER" {print $1}')
for id in ${myarr[*]}; do
docker inspect --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}{{println .Name}}' $id
done