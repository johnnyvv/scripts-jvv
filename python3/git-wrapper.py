#class for git
import os
import subprocess
from sys import exit

class GitWrapper:

    WARNING = '\033[93m'

    def __init__(self, git_dir, feature_branch, protected_branches, master_branch):
        print("Class is in directory{}".format(os.getcwd()))
        #testing if the git_dir is indeed a git directory / exists
        self.git_dir = git_dir if os.path.isdir(git_dir+'/'+'.git') else print("{} is not a valid git directory\nExiting...".format(git_dir))
        self.feature_branch = feature_branch
        self.protected_branches = protected_branches
        self.master_branch = master_branch #might make a check to see if the origin indeed has a branch named like the value given
        self.FNULL = open(os.devnull,'w')
        os.chdir(self.git_dir)

    @staticmethod
    def call_command(args):
        #this method takes an argument  and executes that command without capturing the output.
        subprocess.call(args,shell=False)

    #@staticmethod
    def call_command_ret(self,args):
        #this method returns the output of a given command
        return subprocess.check_output(args,shell=False,stderr=self.FNULL)

    @staticmethod
    def get_current_branch():
        args = ['git', 'branch']
        args2 = ['grep', '*']
        process_git = subprocess.Popen(args, stdout=subprocess.PIPE,
                                        shell=False)
        process_grep = subprocess.Popen(args2, stdin=process_git.stdout,
                                      stdout=subprocess.PIPE, shell=False)
        process_git.stdout.close()
        return process_grep.communicate()[0].decode('utf-8').rsplit('*')[1].strip()

    @staticmethod
    def change_branch(branch_name):
        args = ['git','checkout', branch_name]
        subprocess.call(args)

    def pull_remote_branch(remote_branch = 'master'):
        args = ['git','pull','--no-edit','origin',remote_branch]
        #change to test branch only
        change_branch(test_branch)
        call_command(args)
    def get_remote_commits(cls):
        commit_dict = {}
        args = ['git','ls-remote','--heads']
        remote_branches = cls.call_command_ret(args)
        remote_branches = remote_branches.decode('utf-8').rstrip('\n')
        remote_branches = remote_branches.split('\n')
        for commit in remote_branches:
            branch_info = commit.split('\t')
            commit_dict[branch_info[1].split('/')[2]] = branch_info[0]
        return commit_dict

    def get_local_commits(self):
        commit_dict = {}
        branches = os.listdir(self.git_dir+'/.git/refs/heads')
        for branch in branches:
            with open(self.git_dir+"/.git/refs/heads/"+branch) as f:
                commit_dict[branch] = f.readline().replace('\n','')
        return commit_dict

    def check_branch_update(self, local_commits, remote_commits):
        not_found_arr = []
        for k,v in remote_commits.items():
            if k in local_commits:
                if v == local_commits.get(k):
                    print("Branch {} is up to date.".format(k))
                else:
                    print("Branch {} is not up to date.".format(k)) #can figure out yet if its ahead or behind
                    print("Local commit:\t{}".format(local_commits.get(k)))
                    print("Remote commit:\t{}\n".format(v))
            else:
                not_found_arr.append(k)
                #print("Branch {} not found locally..\n".format(k))
        print("Following branches not found locally:")
        [print(x) for x in not_found_arr]

    def __del__(self):
        #closing the fd you can check this by running process in background and go to the /proc fs
        self.FNULL.close()

def main():
    print("Main is in directory{}".format(os.getcwd()))
    a = GitWrapper('/home/a-veen530/git/pe00024-control','johnny',['production','test'],'master')
    current_branch = a.get_current_branch()
    remote_commits = a.get_remote_commits()
    local_commits = a.get_local_commits()
    a.check_branch_update(local_commits,remote_commits)

if __name__ == '__main__':
    main()