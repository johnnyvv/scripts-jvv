import os,sys,json
from flask import Flask, request, redirect, url_for, render_template, jsonify, abort
from werkzeug.utils import secure_filename
from puppet import PuppetCatalogReader
import jinja2

#prereq
p = PuppetCatalogReader()
UPLOAD_FOLDER = '/tmp/upload'
ALLOWED_EXTENSIONS = set(['json'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


#create a folder /tmp/upload if it doesnt exist(might be deleted inbetween reboots...)
if not os.path.isdir(UPLOAD_FOLDER):
    #creating the folder...
    print("Creating directory...")
    os.mkdir(UPLOAD_FOLDER)

#to do:
# regex text box to search through catalog 


@app.template_test('is_list') # now you can check the condition! test can only give boolean values back..
#use it in the template like {% if <variable> is <name of test> %} e.g. {% if foo is is_list %} # will give foo as an argument to func is_list and return True or False
def is_list(value):
    return isinstance(value,list)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS 
           # check if there is a DOT (.) in the file name and if the right side of the dot equals smth in your allowed ext set.
file_uploaded = []
@app.route('/', methods=['GET','POST'])
def index():
    if request.method == 'POST':
        if 'file' not in request.files: # check if file is in request.files..
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file_uploaded.append(file.filename)
            print(file_uploaded)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
            p.catalog = UPLOAD_FOLDER+'/'+file.filename #set the new catalog
            return redirect(url_for('main'))
        return redirect(url_for('index'))
    return render_template("index_form.html")

@app.route('/main', methods=['GET','POST'])
def main():
    if request.method == 'POST':
        action = request.form['action']
        if action == "test":
            return render_template("summary.html",arr=p.catalog_summary())
        elif action == "numResources":
            return render_template("single.html",title="Number of resources", text_to_return="Number of resources: {}".format(str(p.get_number_resources())))
        elif action == "file":
            return jsonify(p.catalog)
        elif action == "typeResources":
            return render_template("single.html",title="Types in catalog", text_to_return=list(p.get_resource_types()))
        return request.form['action']
    if not len(file_uploaded): # len equals to 0 then throw abort...
        abort(404)
    return render_template("index.html", disabled="", current_file=file_uploaded[0] )

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

app.run(debug=True, host="0.0.0.0")