import socket,struct

#create a tcp socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
rm_addr = ''
port =  #remote port
rm_server = (rm_addr,port)

#send reset
s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER,struct.pack('ii', 1, 0))

try:
    print 'Connecting..'
    s.connect(rm_server)
    print 'connected..'
except socket.error:
    print "Error connection to remote host"
finally:
    print "Exiting, and closing the socket.."
    s.close()