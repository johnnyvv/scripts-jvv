import socket,struct

#create a tcp socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
rm_addr = '10.207.49.24'
ports=['8014','8015']

#send reset
s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER,struct.pack('ii', 1, 0))
s.settimeout(5)

def try_connect(rm_server):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_LINGER,struct.pack('ii', 1, 0))
    s.settimeout(5)
    result = False
    try:
        s.connect(rm_server)
        print 'connected'
        result = True
    except socket.error:
        print "Error connecting to remote host"
    finally:
        s.close()
    return result
s.close()

for port in ports:
    rm_server = (rm_addr,int(port))
    print port
    try_connect(rm_server)