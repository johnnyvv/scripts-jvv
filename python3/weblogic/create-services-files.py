#!/bin/python3
import os,sys

if len(sys.argv) != 4:
    print('Received {} argument(s). Need 3 arguments to run the script.'.format((len(sys.argv)-1)))  
    print('Usage: {} environment domain version'.format(sys.argv[0]))
    sys.exit(1)
else:
    pass
environment=sys.argv[1]
domain=sys.argv[2].upper()
version=sys.argv[3]

if environment == 'production':
    oracle_dir='/apps/oracle'
elif environment == 'acceptance' or environment == "acc":
    oracle_dir='/apps/oracle_acc'
elif environment == 'test':
    oracle_dir='/apps/oracle_test'
else:
    print('No valid environment, exiting...')
    sys.exit(0)
script_homes={
	"production": "/home/oracle/scripts",
	"acc": "/home/oracle/scripts/acc",
	"test": "/home/oracle/scripts/test"
}
nodemanager=True
if version == "12.1.1":
	print("Please notice that with version {} the location of the nodemanager scripts are different.\nplease do't use this script to generate servicefiles for 12.1.1 nodemanager".format(version))
	print("Continuing with making adminserver / managed server service files.")
	nodemanager=False
	#sys.exit(0)

domain_home={
'12.1.1': oracle_dir+'/12-1-1/Middleware/user_projects/domains/domain_'+domain,
'12.1.3': oracle_dir+'/12-1/Middleware/user_projects/domains/domain_'+domain,
'12.2': oracle_dir+'/Middleware/user_projects/domains/domain_'+domain}

def printNodemanager():
    text="""[Unit]
Description={appl_name} nodemanager
Requires=network-online.target
After=network-online.target network.target
Before={appl_name}-adminserver.service

[Service]
Type=simple
KillMode=process
ExecStart={location}/bin/startNodeManager.sh
ExecStop={location}/bin/stopNodeManager.sh
User=oracle
Group=dba

[Install]
WantedBy=multi-user.target
""".format(appl_name=domain,location=domain_home.get(version))
    return text

def printAdmin():
    text="""[Unit]
Description=WLS adminserver {appl_name}
After=network-online.target network.target {appl_name}-nodemanager.service
Wants=network-online.target {appl_name}-nodemanager.service

[Service]
Type=oneshot
ExecStart=/home/oracle/scripts/invokeWLST.sh {script_home}/{appl_name}/controlservers.py startadmin {version}
ExecStop=/home/oracle/scripts/invokeWLST.sh {script_home}/{appl_name}/controlservers.py stopadmin {version}
RemainAfterExit=true
User=oracle
Group=dba

[Install]
WantedBy=multi-user.target""".format(appl_name=domain,version=version,script_home=script_homes.get(environment))
    return text

def printManaged():
    text="""[Unit]
Description=WLS managed server {appl_name}
After=network-online.target network.target {appl_name}-nodemanager.service {appl_name}-adminserver.service
Wants=network-online.target {appl_name}-nodemanager.service {appl_name}-adminserver.service

[Service]
Type=oneshot
ExecStartPre=/bin/sleep 10
ExecStart=/home/oracle/scripts/invokeWLST.sh {script_home}/{appl_name}/controlservers.py startcluster {version}
ExecStop=/home/oracle/scripts/invokeWLST.sh {script_home}/{appl_name}/controlservers.py stopcluster {version}
RemainAfterExit=true
User=oracle
Group=dba

[Install]
WantedBy=multi-user.target""".format(appl_name=domain,version=version,env=environment,script_home=script_homes.get(environment))
    return text

def writeContent(type):
	file = open(domain+"/"+domain+"-"+type+".service", 'w')
	if type == "nodemanager" and nodemanager:
		file.write(printNodemanager())
		file.close()
	elif type == "managedservers":
		file.write(printManaged())
		file.close()
	elif type == "adminserver":
		file.write(printAdmin())
		file.close()
	else:
		print("Wrong type. cant continue")
		sys.exit(1)


if not os.path.exists(domain):
	print("Creating directory {}..".format(domain))
	os.makedirs(domain)
else:
	print("Directory {} already exists..".format(domain))

#print printNodemanager()
if nodemanager:  # if nodemanager is true, create service file. Else skip it. nodemanager is set to false if version == 12.1.1
    writeContent("nodemanager")
writeContent("managedservers")#run these 2 functions always, so dont put them in the if clause
writeContent("adminserver")