#!/bin/env python3

#to do:
# read from csv and add to array instead of src/dst filename
import os, sys, shutil,hashlib
base_dir = '' 
dest_dirs = []
#/home/a-veen530/git/pe00024-control/hieradata/nodes

src_filename = []
dst_filename = []

def create_dict(arr1,arr2):
    if isinstance(arr1,list) and isinstance(arr2,list):
        if len(arr1) == len(arr2):
            i = 0
            server_dict = {}
            while i < len(arr1):
                server_dict[src_filename[i]] = dst_filename[i]
                i+= 1
            return server_dict
        else:
            print("Array are not the same length! array 1 has a length of {} and array 2 has a length of {}".format(len(arr1),len(arr2)))
            sys.exit(1)
    else:
        print("Require two arrays as function arguments, please check your code")
        sys.exit(1)

def search_copy(path,server_dict):
    copied=0
    for src_server,dst_server in server_dict.items():
        if os.path.isfile(path+'/'+src_server+'.yaml'):
            #print("Copying {} to {}".format(path+src_server+'.yaml',path+'/'+dst_server+'.yaml'))
            shutil.copy2(path+'/'+src_server+'.yaml',path+'/'+dst_server+'.yaml')
            copied+= 1
        else:
            print("No yaml file found for server: {}".format(src_server))
    return copied

def get_file_hash(path):
    if not os.path.isfile(path):
        print("Path: {} does not exists.".format(path))
        sys.exit(1)
    with open (path,'rb') as input_file:
        #print(hashlib.md5(input_file.read()).hexdigest())
        return hashlib.md5(input_file.read()).hexdigest()

def check_file_integrity(path,server_dict,remove_bad_files=False):
    files_mismatched = []
    errors=0
    for src_server,dst_server in server_dict.items():
        if get_file_hash(path+'/'+src_server+'.yaml') == get_file_hash(path+'/'+src_server+'.yaml'):
            print("Hash is the same for servers {} <--> {}".format(src_server,dst_server))
        else:
            errors+= 1
            if remove_bad_files:
            #files_mismatched.append(path+'/'+src_server+'.yaml')
            print("ERROR! files are not the same for servers {} <--> {} at path: {}".format(src_server,dst_server,path))
            print("Removing file {}".format(path+'/'+src_server+'.yaml'))
            os.remove(path+'/'+src_server+'.yaml')
            else:
                print("ERROR! files are not the same for servers {} <--> {} at path: {}".format(src_server,dst_server,path))
    return errors
    
#print(create_dict(src_filename,dst_filename))
server_dict = create_dict(src_filename,dst_filename)
#for key,value in server_dict.items():
#    print("{} -> {}".format(key,value))


files_copied = 0
for path in dest_dirs:
    files_copied += search_copy(base_dir+path,server_dict)
print("Copied {} file(s).".format(files_copied))

print("Checking integrity of the files....")
file_errors = 0 #checking amount of servers where the files do not match
for path in dest_dirs:
    file_errors += check_file_integrity(base_dir+path,server_dict)
if file_errors == 0:
   print("All destination files match with their source")
else:
   print("Errors found, please check your logging")