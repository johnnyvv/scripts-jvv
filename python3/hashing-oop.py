import os
import sqlite3
import hashlib
import random,string

class User():
	DATABASE = "oop-hash-users.db"

	def __init__(self, username,password=None):
		print(self)
		self.username = username
		self.password = password
		print(User.DATABASE)
		if not os.path.isfile(User.DATABASE):
			print("Creating database")
			self._db = sqlite3.connect(User.DATABASE)
			self._cursor = self._db.cursor()
			self.initialize_database()
		else:
			print("already a database")
			self._db = sqlite3.connect(User.DATABASE)
			self._cursor = self._db.cursor()
		if not password:
			print("Creating new user.. Password is None..")
			self.create_user()
			print("Logging in, please enter password for {}".format(self.username))
			self.password = User.get_password()
			self.login()
		else:
			self.login()

	def initialize_database(self):
		query_schema_create = """
		CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT unique NOT NULL, password TEXT NOT NULL, password_salt TEXT NOT NULL, group_id INTEGER NOT NULL)""" #DEFAULT 0)"""
		try:
			self._cursor.execute(query_schema_create)
			self._db.commit()
		except sqlite3.Error as e:
			print("An error occured: ", e.args[0])
			self._db.rollback()

	def select_user(self):
		self._cursor.execute("select * from users WHERE username = ?",(self.username,))
		fetched_user = self._cursor.fetchone()
		if not fetched_user:
			print("user {} not found.".format(self.username))
			raise("User {} not found".format(self.username))
		else:
			print("ok")
			return fetched_user

	def create_user(self):
		self.password = User.get_password()
		user_salt = User.generate_salt()
		try:
			self._cursor.execute("INSERT INTO users VALUES(?,?,?,?,?)",(None,self.username,User.generate_hash(user_salt,self.password),user_salt,0)) # insert into the database.. First arg None is because sqlite is auto incrementing the database..
			self._db.commit()
		except sqlite3.IntegrityError as e:
			print("Cant add {}, it's not unique!".format(self.username))
			print(e.args[0])

	def login(self):
		fetch_user = self.select_user()
		print(type(fetch_user))
		user_salt = fetch_user[3] # using indexes, salt is the 4th column in the users table
		print(user_salt)
		print("Checking if the password is the same as hash in database...") 
		password_hashed = User.generate_hash(user_salt,self.password)
		print(fetch_user[2]) # print the hash in the database..
		print(password_hashed)
		if password_hashed == fetch_user[2]: # checking if passwords are equal.. 
			print("A match made in sqlite3..")
			self.logged_in = True
			return self.logged_in
		else:
			print("Wrong username or")
			self.logged_in = False
			return self.logged_in

	def am_logged_in(self):
		return self.logged_in

	@staticmethod
	def checkStringNotEmpty(text_input):
		while(True):
			to_return = input(text_input)
			if not to_return:
				continue
			else:
				return to_return
				break

	@staticmethod
	def get_username(input_string="Enter your username:"):
		return User.checkStringNotEmpty(input_stringin)

	@staticmethod
	def get_password(input_string="Enter password:"):
		return User.checkStringNotEmpty(input_string)

	@staticmethod #use static if you not using the function to manipulate the class ( class method ) or instance data (self)
	def generate_salt(SALT_CHARS=8):
		generated_salt = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(SALT_CHARS)).encode('utf-8') #generate SALT_CHARS (n) salt for the user
		return generated_salt

	@staticmethod
	def generate_hash(salt,password):
		salt = salt if isinstance(salt, bytes) else salt.encode('utf-8') # check if salt is of type bytes or b'string' if not encode it so it is.
		password = password if isinstance(password,bytes) else password.encode('utf-8') #same as above
		hasher = hashlib.sha512() #create a new object for hashing
		hasher.update(salt+password) # add the salt and password to the hash
		return hasher.hexdigest() #generate the hash 

	def echo_user(self):
		return self.username

def main():
	x = User("johnny","<password>")
	print(x.echo_user())
	print(x.am_logged_in())
	#print(User.generate_salt()) # access the static method
	#print(User.generate_hash(User.generate_salt(),"password"))
	y = User("<password>")
	print(y.echo_user())
	print(y.am_logged_in())

	z = User("Pucky")

if __name__ == '__main__':
	main()