#!/bin/python
from subprocess import call,check_output
import crypt,random,string,sys

#print crypt.crypt("test", "$6$random_salt")
hash="$6$"
added_users=0
sudo_group="sudo"

users={ "test": "test", "test2": "test"}
#current_user_id=check_output(["id", "-u"])#, shell=True).strip()
current_user=check_output("/usr/bin/whoami", shell=False).strip()
print current_user
if current_user == "root":
    print 'User is root, continue'
else:
    print 'User is not equal to root, checking your groups for sudo defined sudo group....'
    groups=check_output(["groups",current_user])
    print groups
    sys.exit(1)

for key, value in users.items():
  added_users+=1
  rand=''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(8))
  print "Adding user:", key
#  print rand
  call(["useradd", key])
  print "changing password"
  print "lpasswd", key , "-p", crypt.crypt(value,hash+rand)
  call(["lpasswd", key, "-p", crypt.crypt(value,hash+rand)])

print "Completed, added %d user(s)" % added_users
