#!/bin/python
try:
    import sys, logging, os, getopt,ConfigParser
except ImportError:
    print 'Failed to import a module.'


#import properties from a file, so that this file can be reused. Now every domain has it's own custom file. BAD! 

#add argument so user can specify which propery file has to be used. 

opts,args = getopt.getopt(sys.argv[1:], "f:", ["file="])

for o,a in opts:
    if o in ['-f', '--file'] and os.path.isfile(a):
        ini_file = a


#create configparser object to read the ini file
config = ConfigParser.ConfigParser()
config.read(ini_file)
#get all key-value pairs fron the section 'config', just access the data like you would with any other dictionary.
#if you want to do additonal checks on the value / type of a certain key, it might be better to use the function getboolean etc, check manual for how to
#we cant use the code beaneath, because WLST is using an version w/o the .items function
#ini_data = dict(config.items('config'))
#to reselve we weill do it a bit dirrent..
options = config.options('config')
ini_data = {}
for option in options:
    ini_data[option] = config.get('config',option) 

#wls_username is not being used actively, because of the login which only require .secure files.
#wls_host is used to connect to the admin server
#wls_port is used to connect to the admin server
#nm_port is used to connect to the nodemanager
#nm_host is used to connect to the nodemanager
#listen_type is used to set the type of connection to use when connecting to the nodemanager
#domain is used to set the right paths etc..
#environment used to set the right paths etc...
#wls_version is not being used
#shutdown_force whether or not the server should be shutdown using forece


#password=''
domain_prefix='domain_'
admin_server='AdminServer_'+ ini_data['domain']
cluster_name="CL_"+ ini_data['domain']

if ini_data['environment'] == 'production':
    oracle_dir='/apps/oracle'
elif ini_data['environment'] == 'acceptance':
    oracle_dir='/apps/oracle_acc'
elif ini_data['environment'] == 'test':
    oracle_dir='/apps/oracle_test'
else:
    print 'No valid environment, exiting...'
    sys.exit(0)

domain_home={
'12.1.1': oracle_dir+'/12-1-1/Middleware/user_projects/domains/domain_'+ini_data['domain'],
'12.1.3': oracle_dir+'/12-1/Middleware/user_projects/domains/domain_'+ini_data['domain'],
'12.2': oracle_dir+'/Middleware/user_projects/domains/domain_'+ini_data['domain']}

config_store={
    "production": "/home/oracle/scripts",
    "acceptance": "/home/oracle/scripts/acc",
    "test": "/home/oracle/scripts/test"
}

user_file=config_store.get(ini_data['environment'])+'/'+ini_data['domain'].upper()+'/'+ini_data['domain']+'-configfile.secure'
key_file=config_store.get(ini_data['environment'])+'/'+ini_data['domain'].upper()+'/'+ini_data['domain']+'-keyfile.secure'
#nm_user_file=config_store.get(environment)+'/'+domain+'/'+domain+'-nodemanager-configfile.secure'
#nm_user_key=config_store.get(environment)+'/'+domain+'/'+domain+'-nodemanager-keyfile.secure'
#logfile='/tmp/log-'+ini_data['domain']
logdir = config_store.get(ini_data['environment'])+'/'+ini_data['domain'].upper()
logfile = logdir + '/' + ini_data['domain'] + '.log'
loglevel='INFO'

def connectToNodeManager():
    print "nmConnect(userConfigFile="+user_file+",userKeyFile="+key_file+",host="+ini_data['nm_host']+",port="+ini_data['nm_port']+",domainName="+domain_prefix+ini_data['domain']+"nmType="+ini_data['listen_type']+")"
    if os.path.isfile(user_file) and os.path.isfile(key_file):
        try:
            #nmConnect(nm_username,password,nm_host,nm_port,domain_prefix+domain,domain_home.get(wls_version),listen_type)
            nmConnect(userConfigFile=user_file,userKeyFile=key_file,host=ini_data['nm_host'],port=ini_data['nm_port'],domainName=domain_prefix+ini_data['domain'],nmType=ini_data['listen_type'])
            #nmConnect('weblogic','','localhost','domain_OSB','/apps/oracle/Middleware/user_projects/domains/domain_OSB','ssl')
        except:
            print 'Failed to connect to nodemanager. Exiting..'
            sys.exit()
    else:
        print 'No key or userconfig file found for domain %s. Exiting' % ini_data['domain']
        logger.info('Could not connect to nodemanager, as the server has no userconfig file or keyfile.')
        sys.exit(1)         

def checkNodeManagerStatus():
    nmStatus = nm()
    if nmStatus == "Not connected to Node Manager" and not nmStatus:
        print 'Not connected to a nodemanager. Exiting'
        sys.exit()
    else:
        return True

def disconnectFromNodeManager():
    if checkNodeManagerStatus():
        nmDisconnect()
    else:
        print 'Cant disconnect from nodemanager as there is not active connection to it'
        sys.exit(1)

def connectToAdminServer():
    if os.path.isfile(user_file) and os.path.isfile(key_file):
        try:
            connect(userConfigFile=user_file,userKeyFile=key_file,url='t3://'+ini_data['wls_host']+':'+ini_data['wls_port'])
        except:
            print 'Failed to connect to admin server %' % getServerNames('admin')
    else:
        print 'No key or userconfig file found for domain %s. Exiting' % ini_data['domain']
        logger.info('Could not connect to admin server, as the server has no userconfig file or keyfile.')
        sys.exit(1)

def startAdminServer():
    if checkNodeManagerStatus():
        stat = nmServerStatus(admin_server)
        #stat = stat.replace(" ","")
        if stat == "RUNNING":
            print 'AdminServer is already running.'
            logger.info('Tried to start AdminServer, but admin server is already in %s state.' % stat)
        elif stat == "UNKNOWN" or stat == "FAILED_NOT_RESTARTABLE":
            try:
                print 'Trying to start admin server, as state does not equal "SHUTDOWN"'
                print 'Actual state of AdminServer is %s' % stat 
                log.info('State is equal to UNKNOWN or FAILED_NOT_RESTARTABLE, trying to start AdminServer anyway....')
                logger.info('Starting %s, current state of the server is:%s' % (admin_server,stat))
                nmStart(admin_server)
                logger.debug('nmStart command invoked.')
            except:
                print 'Cant start AdminServer, please check log files! Perhaps server started without nodemanager.'
                print 'State of AdminServer = %s' % stat
                sys.exit(1)
        else:
            print 'Starting '+admin_server
            logger.info('Starting %s, current state of the server is:%s' % (admin_server,stat))
            nmStart(admin_server)

def stopAdminServer():
    stat = getServerStatus(admin_server)
    if stat == "RUNNING":
        print 'AdminServer is running, shutting down'
        logger.info('Shutting down %s with force is %s' % (admin_server,ini_data['shutdown_force']))
        shutdown(name=admin_server,force=ini_data['shutdown_force'])
        logger.debug('Shutdown command invoked with force is %s' % ini_data['shutdown_force'])
    else:
        print 'Server is already down, %s' % stat
        logger.info('%s is not in a RUNNING state, actual state is %s' (admin_server,stat))

def statusAdmin():
    print 'State of admin server %s is: %s ' % (admin_server,str(getServerStatus(admin_server)))
    logger.info('State of admin server %s is: %s ' % (admin_server,str(getServerStatus(admin_server))))

#we want to start a server that is specified. 
def startManagedServer(server):
    managed_servers=getServerNames("ms")
    if server in managed_servers:
        if getServerStatus(server) == "RUNNING":
            print 'cant start server, %s is already running.' % server
        else:
        #start the server
            start(server)
    else:
        #raise exception
        print 'Server %s not found.' % server

def startManagedServer_nm(server):
    current_state = nmServerStatus(server)
    if current_state == "UNKNOWN":
        print 'Could not get status of %s, most likely you are not connected to the proper nodemanager or the server does not exist.' % server
        logger.info('Could not get status of %s, most likely you are not connected to the proper nodemanager' % server)
    elif current_state == "RUNNING":
        print 'cant start server, %s is already running.' % server
        logger.info('Server %s is already running' % server)
    else:
        logger.info('Starting %s with nodemanager.' % server)
        nmStart(server)

def startManagedServers():
    managed_servers=getServerNames("ms")
    admin_state=getServerStatus(admin_server)
    for ms in managed_servers:
        current_state=getServerStatus(ms)
        if current_state == "RUNNING":
             print '%s is already running -> skipping..' % ms
             logger.info('Can\'t start managed server %s, is already in %s state.' % (ms,current_state))
        elif admin_state is not 'RUNNING':
            print 'Admin server is not running.'
            logger.info('Admin server is not running, cant start managed servers.')
            sys.ext(1)
        else:
            print 'starting ms %s ' % ms
            logger.info('Starting managed server %s' % (ms))
            start(ms)
            logger.debug('Invoked start command for managed server %s' % ms)

def startCluster():
    logger.info('Starting cluster %s' % cluster_name)
    start(cluster_name,"Cluster")
    logger.debug('Invoked start command for cluster %s' % cluster_name)

def stopCluster():
    logger.info('Stopping cluster %s' % cluster_name)
    shutdown(name=cluster_name,entityType='Cluster',force=ini_data['shutdown_force'])
    logger.debug('Invoked stop command for cluster %s' % cluster_name)

def statusCluster():
    #logger.info('State of cluster is '+str(state(cluster_name,'Cluster')))
    state(cluster_name,'Cluster')
    #pass # instead of using a loop, you can also start a cluster. Check the manual for starting a cluster.

def stopManagedServer(server):
    managed_servers=getServerNames("ms")
    current_state = getServerStatus(server)
    print current_state
    if server in managed_servers:
        logger.info("Trying to stop %s." % server)
        if current_state != "RUNNING":
            logger.info("Can't stop server %s, as it's not running. Status is: %s" % (server,current_state))
            print 'cant stop server, %s is not RUNNING.' % server
        else:
            logger.info('Shutting down %s.' % server)
            shutdown(name=server,force=ini_data['shutdown_force'])
    else:
        #raise exception
        logger.info("Tried to shutdown %s, but the server is not found." % server)
        print 'Server %s not found.' % server        

def stopManagedServer_nm(server):
    current_state = nmServerStatus(server)
    if current_state == "UNKNOWN":
        print 'Could not get status of %s, most likely you are not connected to the proper nodemanager or the server does not exist.' % server
        logger.info('Could not get status of %s, most likely you are not connected to the proper nodemanager' % server)
    elif current_state == "RUNNING":
        logger.info('Stopping %s.' % server)
        print 'Stopping %s.' % server
        nmKill(server)
    else:
        logger.info("Failed to stop %s as the state is:%s" % (server,current_state))
        print "Failed to stop %s as the state is:%s" % (server,current_state)

def stopManagedServers():
    managed_servers=getServerNames("ms")
    for ms in managed_servers:
        current_state=getServerStatus(ms)
        if current_state == "SHUTDOWN":
             print '%s is already shutdown -> skipping..' % ms
             logger.info('Current stat is managed server %s is %s' % (ms,current_state))
        else:
             print 'stopping ms %s ' % ms
             logger.info('Stopping managed server %s' % ms)
             print 'state of ms %s ' % current_state
             shutdown(name=ms,force=ini_data['shutdown_force'])
             logger.debug('Invoked shutdown command for %s' % ms)

def getServerStatus(server):
    domainRuntime()
    serverstate=cmo.lookupServerLifeCycleRuntime(server)
    return serverstate.getState()

def getManagedServerStatus():
    managed_servers=getServerNames("ms")
    for ms in managed_servers:
        print 'State of server %s is: %s ' % (ms,str(getServerStatus(ms)))
        logger.info('State of managed server %s is: %s ' % (ms,str(getServerStatus(ms))))

def getServerNames(type): # type should be ms if you want to get the managed servers or admin if you want to get the admin server..
    found_msservers=[]
    found_adminserver=[]
    found_servers=cmo.getServers() #get the servers for a specified domain
    for server in found_servers:
        if 'Admin' in server.getName():
            #if admin is found, then it's prolly an admin server..
            found_adminserver.append(server.getName())
        else: #if server name does not contain 'Admin' then it's prolly a managed server   #"MS_"+ ini_data['domain'] in server.getName(): # if name is not like 'Admin' then it's prolly a managed server.
            found_msservers.append(server.getName())

    if type == "ms":
        return tuple(found_msservers)
    elif type == "admin":
        return tuple(found_adminserver)
    else:
        print "server type %s not found." % type
        sys.exit(1)

def configureLogging():
    if os.path.isdir(logdir):
        logging.basicConfig(filename=logfile,format='%(asctime)s %(message)s',level=getattr(logging,loglevel),datefmt='%d-%m-%Y %H:%M:%S')
        logger = logging.getLogger(__name__)
        return logger
    else:
        print 'The log directory %s does not exist. Exiting....' % logdir

    #handler = logging.FileHandler(logfile)
    #handler.setLevel(getattr(logging,loglevel))
    #formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    #handler.setFormatter(formatter)
    #logger.addHandler(handler)
    

pos_actions = ['startallms','startms','stopallms','stopms','statusms','startcluster','stopcluster','statuscluster','startadmin','stopadmin','statusadmin','startms_nm','stopms_nm']

if sys.argv[3] in pos_actions and len(sys.argv) <=5:
    action = sys.argv[3]
    logger = configureLogging() #set up logging..

    if action == "startallms":
        #ms_to_start=raw_input('Name of server that needs to be started:')
        connectToAdminServer()
        startManagedServers()
        disconnect()
        #werkt
    elif action == "startms":
        if len(sys.argv) < 5:
            print 'You need to specify the server you wish to start, exiting...'
            sys.exit(1)
        connectToAdminServer()
        startManagedServer(sys.argv[4])
        #werkt
    elif action == "startms_nm":
        if len(sys.argv) < 5:
            print 'You need to specify the server you wish to start, exiting...'
            sys.exit(1)
        connectToNodeManager()
        startManagedServer_nm(sys.argv[4]) 
        disconnectFromNodeManager()       
    elif action == "stopallms":
        connectToAdminServer()
        stopManagedServers()
        disconnect()
        #werkt
    elif action == "stopms":
        if len(sys.argv) < 5:
            print 'You need to specify the server you wish to stop, exiting...'
            sys.exit(1)
        connectToAdminServer()
        stopManagedServer(sys.argv[4])
    elif action == "stopms_nm":
        if len(sys.argv) < 5:
            print 'You need to specify the server you wish to stop, exiting...'
            sys.exit(1)
        connectToNodeManager()     
        stopManagedServer_nm(sys.argv[4])
        disconnectFromNodeManager()

    elif action == "statusms":
        connectToAdminServer()
        getManagedServerStatus()
        disconnect()
        #werkt  
    elif action == "stopcluster":
        connectToAdminServer()
        stopCluster()
        disconnect()
    elif action == "statuscluster":
        connectToAdminServer()
        statusCluster()
        disconnect()
    elif action == "startcluster":
        connectToAdminServer()
        startCluster()      
        disconnect()
    elif action == "stop":
        stopWebserver()
        disconnectNodemanager()
    elif action == "startadmin":
        connectToNodeManager()
        startAdminServer()
        disconnectFromNodeManager()
        #werkt
    elif action == "stopadmin":
        connectToAdminServer()
        stopAdminServer()
    elif action == "statusadmin":
        connectToAdminServer()
        statusAdmin()
    else:
        print action+' is not a valid action. startms|startallms <server>|stopms|stopallms <server>|statusms|startcluster|stopcluster|statuscluster|startadmin|stopadmin'

elif len(sys.argv) > 5:
    print 'Please supply one or 2 argv! startms|startallms <server>|stopallms <server>|stopms|statusms|startcluster|stopcluster|statuscluster|startadmin|stopadmin'
    sys.exit(1)

else:
    print 'Please supply an action startms|startallms <server>|stopallms <server>|stopms|statusms|startcluster|stopcluster|statuscluster|startadmin|stopadmin '
    sys.exit(1)