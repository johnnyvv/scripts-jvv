#!/bin/python
import sys
username='weblogic'
password=''.decode('base64')
host='localhost'
port='5556'
domain='domain_WLS' #name of the ohs instance
listen_type='ssl'
domain_home='/apps/oracle/Middleware/user_projects/domains/domain_WLS'
def connecToNodeManager(username,password,host,port,domain,domain_home,listen_type):
    try:
        nmConnect(username,password,host,port,domain,domain_home,listen_type)
    except:
        print 'Failed to connect to nodemanager. Exiting..'
        sys.exit()

def checkNodeManagerStatus():
    nmStatus = nm()
    if nmStatus == "Not connected to Node Manager" and not nmStatus:
        print 'Not connected to a nodemanager. Exiting'
        sys.exit()
    else:
        return True

def startWebserver():
    if checkNodeManagerStatus():
        print 'Starting webserver...'
        nmStart(serverName='ohs_WLS',serverType='OHS')

def restartWebserver():
    if checkNodeManagerStatus():
        print 'Restarting webserver...'
        nmSoftRestart(serverName='ohs_WLS',serverType='OHS')

def serverStatus():
    if checkNodeManagerStatus():
        print 'Status of OSB webserver:'
        nmServerStatus(serverName='ohs_WLS',serverType='OHS')

def stopWebserver():
    if checkNodeManagerStatus():
        print 'Status of OSB webserver:'
        nmKill(serverName='ohs_WLS',serverType='OHS')

if len(sys.argv) == 2:
    action=sys.argv[1]
    connecToNodeManager(username,password,host,port,domain,domain_home,listen_type)
    if action == "start":
        startWebserver()
    elif action == "restart":
        restartWebserver()
    elif action == "status":
        serverStatus()
    elif action == "stop":
        stopWebserver()
    else:
        print action+' is not a valid action. start|stop|restart|status'
elif len(sys.argv) > 2:
    print 'Please supply only one argv! start|stop|restart|status'
    sys.exit()
else:
    print 'Please supply an action start|stop|restart'
    sys.exit()