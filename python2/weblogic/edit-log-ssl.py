key_password = '<password for the keystore / keypairs, here they have the same pw!>'
outbound_key_alias = '<alias of keypair to be used for two way ssl>'
ssl_key_alias = '<alias of server key>'
keystore_location = ''
trust_location = ''
wls_username = 'weblogic' #pretty default, change accordingly 
wls_password = ''
wls_url = 'localhost:8001'
tls_version='TLSv1.2'

connect(wls_username,wls_password, wls_url)

#root directory of domain, we set it above the functions so we don't need to pass it as an argument to it, make sure you are connected to domain before calling get(x)
domainRoot = get('RootDirectory')
domainName = get('Name')

servers=cmo.getServers()
serverlist=[]
for server in servers:
    serverlist.append(server.getName())

#this ssl sets the outbound ssl key+pair to use(two way ssl), and esets the keypair which will be used by the server(incoming connections)
def editSsl(server):
        cd('/Servers/'+server)
        set('KeyStores', 'CustomIdentityAndCustomTrust')
        cd('/Servers/'+server+'/SSL/'+ server)
        set ('UseClientCertForOutbound',True)
        set ('UseServerCerts',True)
        set ('ClientCertAlias',outbound_key_alias)
        set ('ClientCertPrivateKeyPassPhrase', key_password)
        set('HostnameVerificationIgnored',true)
        set('ServerPrivateKeyAlias', ssl_key_alias)
        set('ServerPrivateKeyPassPhrase', key_password)
        set('MinimumTLSProtocolVersion',tls_version)
        cd('/Servers/'+ server)
        #setting ssl port key
        set('CustomIdentityKeyStoreFileName', keystore_location)
        set('CustomIdentityKeyStorePassPhrase', key_password)
        set('CustomIdentityKeyStoreType', 'jks')
        set('CustomTrustKeyStoreFileName', trust_location)
        set('CustomTrustKeyStorePassPhrase',key_password)
        set('CustomTrustKeyStoreType', 'jks')
        save()

def editLog(server):
    #set default logging
    cd('/Servers/'+server+'/Log/'+server)
    set('FileName',domainRoot+'/servers/'+server+'/logs/'+server+'.log')
    set('RotationType','byTime')
    set('RotationTime','00:00')
    set('FileCount','365')
    set('LogFileRotationDir',domainRoot+'/servers/'+server+'/logs/rotated')
    #set access log
    cd('/Servers/'+server+'/WebServer/'+server+'/WebServerLog/'+server)
    set('FileName',domainRoot+'/servers/'+server+'/logs/access.log')
    set('NumberOfFilesLimited', True)
    set('RotationType','byTime')
    set('RotationTime','00:00')
    set('FileCount','31')
    set('LogFileRotationDir',domainRoot+'/servers/'+server+'/logs/rotated/access')
    #set domain log file, location will be the adminserver log location.
    if server == 'AdminServer':
        cd('/Log/'+domainName)
        set('FileName',domainRoot+'/servers/'+server+'/logs/'+domainName+'.log')
        set('RotationType','byTime')
        set('RotationTime','00:00')
        set('FileCount','31')
        set('LogFileRotationDir',domainRoot+'/servers/'+server+'/logs/rotated/domain')
    save()

edit()
startEdit()
for server in serverlist:
    if 'BSSV' in server:
        editSsl(server)
        editLog(server)
    elif 'AdminServer' in server:
        editLog(server)
activate()