#!/bin/python
#generating user config files
import sys,os
if len(sys.argv) == 4:
    pass
else:
    print "Usage: %s <environment> <host> <password>" % sys.argv[0]
    print "Valid values:"
    print "environment -> production|acceptance|test"
    print "Example: <wlst_bin>/wlst.sh storeUserConfig.py production 10.2.2.9 my@psoword"
    sys.exit(1)

domains=["00OSB","01WPP","02DFD","03DIV","04REL"] #,"02DFD","04REL"]
#domains=["04REL"]
environment=sys.argv[1]
host=sys.argv[2]
username="weblogic"
password=sys.argv[3]
#domain=sys.argv[2]

last_char={
        "production": "P",
        "acceptance": "A",
        "test": "T"
}
char=last_char.get(environment)

for index,domain in enumerate(domains): #for each domain in the domains array, add the last char to the end.
        print index , " >> " , domain
        domains[index] = str(domain+char)

if environment == 'production':
    oracle_dir='/apps/oracle'
elif environment == 'acceptance':
    oracle_dir='/apps/oracle_acc'
elif environment == 'test':
    oracle_dir='/apps/oracle_test'
else:
    print 'No valid environment, exiting...'
    sys.exit(0)


nm_type={
        "00OSB"+char: "ssl",
        "01WPP"+char: "ssl",
        "02DFD"+char: "ssl",
        "03DIV"+char: "ssl",
        "04REL"+char: "plain"
        }

domain_version={
        "00OSB"+char: "12.2",
        "01WPP"+char: "12.1.3",
        "02DFD"+char: "12.1.3",
        "03DIV"+char: "12.2",
        "04REL"+char: "12.1.1"
        }

if environment == "production" or environment == "acceptance":
        admin_ports={
        "00OSB"+char: 7001,
        "01WPP"+char: 7011,
        "02DFD"+char: 7021,
        "03DIV"+char: 7031,
        "04REL"+char: 7051
        }

        nm_ports={
        "00OSB"+char: 5000,
        "01WPP"+char: 5001,
        "02DFD"+char: 5002,
        "03DIV"+char: 5003,
        "04REL"+char: 5005
        }

if environment == "test":
        admin_ports={
        "00OSB"+char: 5001,
        "01WPP"+char: 5011,
        "02DFD"+char: 5021,
        "03DIV"+char: 5031,
        "04REL"+char: 5051
        }

        nm_ports={
        "00OSB"+char: 5550,
        "01WPP"+char: 5551,
        "02DFD"+char: 5552,
        "03DIV"+char: 5553,
        "04REL"+char: 5555
        }

def connect_admin(domain_def):
        #print username + password + "t3://"+host+":"+str(admin_ports.get(domain_def))
        #print '('+username+','+password+',t3://'+host+':'+str(admin_ports.get(domain_def))
        try:
            connect(username,password,"t3://"+host+":"+str(admin_ports.get(domain_def)))
        #redirect('/dev/null','false')
        except:
            print "ERROR could not connect to adminserver for domain: %s" % domain_def

def check_userconfig_directory_exists(path):

        if os.path.isdir(path):
            return True
        else:
            print 'creating directory for domain %s' % domain
            os.makedirs(path)

def check_userconfig_exists(domain_def):
        userconfig_path = config_store.get(environment)+"/"+domain_def.upper()
        print userconfig_path

        if check_userconfig_directory_exists(userconfig_path):
            print 'Directory exists.'


        if os.path.isdir(userconfig_path): # if true, continue and check files...
            print 'directory for domain %s exists.' % domain_def
            if os.path.isfile(userconfig_path+"/"+domain_def+"-configfile.secure") and os.path.isfile(userconfig_path+"/"+domain_def+"-keyfile.secure"):
                print 'there is already a userconfig and keyconfig stored for domain %s' % domain_def
                return True

            elif os.path.isfile(userconfig_path+"/"+domain_def+"-configfile.secure") or os.path.isfile(userconfig_path+"/"+domain_def+"-keyfile.secure"):
                print 'Either the key or config for for domain %s exists.. skipping' % domain_def
                return True

            else:
                print 'No files found for domain %s creating userconfig....' % domain_def
                #confirmation can be bypassed to use a startup argu for wlst -> Dweblogic.management.confirmKeyfileCreation=false
                connect_admin(domain_def)
                storeUserConfig(userconfig_path+"/"+domain_def+"-configfile.secure",userconfig_path+"/"+domain_def+"-keyfile.secure")

                #if not os.path.isfile(userconfig_path+"/"+domain_def+"nodemanager-configfile.secure") and not os.path.isfile(userconfig_path+"/"+domain_def+"nodemanager-keyfile.secure"):
                #    print 'Also creating nodemanager files..'
                    #print "nmConnect('weblogic'"+password+","+host+","+str(nm_ports.get(domain))+","+domain+","+domain_home.get(domain_version.get(domain))+","+nm_type.get(domain)+")"
                #    nmConnect(username='weblogic',password=password,host=host,port=nm_ports.get(domain),domainName='domain_'+domain,nmType=nm_type.get(domain))
                #    storeUserConfig(userconfig_path+"/"+domain_def+"-nodemanager-configfile.secure",userconfig_path+"/"+domain_def+"-nodemanager-keyfile.secure",nm='true')
                #    nmDisconnect()

for domain in domains:
        #first check if there is a userconfig on the filesystem.
    domain_home={
'12.1.1': oracle_dir+'/12-1-1/Middleware/user_projects/domains/domain_'+domain,
'12.1.3': oracle_dir+'/12-1/Middleware/user_projects/domains/domain_'+domain,
'12.2': oracle_dir+'/Middleware/user_projects/domains/domain_'+domain}

    config_store={
        "production": "/home/oracle/scripts",
        "acceptance": "/home/oracle/scripts/acc",
        "test": "/home/oracle/scripts/test"}

    check_userconfig_exists(domain)
    print 'domain %s done..' % domain
# to test user file: enter wlst connect(userConfigFile='<configfile_location>',userKeyFile='<keyfile_location>',url='t3://<host>:<port>')