import requests
import urllib
import json
import csv

def run_query(query,headers):
        return requests.get(base_url+urllib.urlencode(query),verify=cacert,cert=(public_key,private_key),headers=headers).text

def change_time_format(input_dict):
    for server in input_dict:
        if server['report_timestamp']:
            server['report_timestamp'] = server['report_timestamp'].split('T')[0]+'-'+server['report_timestamp'].split('T')[1].split('.')[0]

def compare_append_match(input_dict,input_array):
    for p_server in input_dict:
        #if p_server['certname'] in [row[0] for row in input_array]: #because hosts_list contains arrays, we need to loop through each array to check if hostname matches.
        #        row.append(p_server['value'].encode('utf-8'))
        for row in input_array:
            if row[0] == p_server['certname']:
                print 'ok %s' % p_server['value']
                row.append(p_server['value'])
    return input_array

def create_dict_from_two(first_dict,second_dict,first_key,second_key):
    arr_return = []
    for f in first_dict:
        for s in second_dict:
            if f['certname'] == s['certname']:
                #print 'ok'
                arr_return.append([str(f['certname']),str(f[first_key]),str(s[second_key])])
    return arr_return

cacert = "/etc/puppetlabs/puppet/ssl/certs/ca.pem"
private_key = "/etc/puppetlabs/puppet/ssl/private_keys/"
public_key = "/etc/puppetlabs/puppet/ssl/certs/"
base_url = ":8081/pdb/query/v4?"
headers = {}
#query = str(sys.argv[1])
query_lastreport = "nodes[certname,report_timestamp]{}"
query_agentversion = "facts[]{ name = 'aio_agent_build'}"
query_env = "facts[certname,value]{ name = 'customer_environment'}"

dic_lastreport = {'query':query_lastreport}
dic_agentversion = {'query':query_agentversion}
dic_env = dic_env = {'query':query_env}


lastreport_json = json.loads(run_query(dic_lastreport,headers))
agentversion_json = json.loads(run_query(dic_agentversion,headers))
env_json = json.loads(run_query(dic_env,headers))

change_time_format(lastreport_json)

result = create_dict_from_two(lastreport_json,agentversion_json,'report_timestamp','value')

with open('test-agent-versions.csv','wt') as csvwrite:
    a = csv.writer(csvwrite)
    a.writerow(('hostname','last_puppet_run','agent_version','environment'))
    [a.writerow(item) for item in  compare_append_match(env_json,result)]
    #[a.writerow(item) for item in compare_append_match(lastreport_json,agentversion_json,'report_timestamp','value')]