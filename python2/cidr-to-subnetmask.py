#!/bin/env python
#this script calculates the cidr or converts cidr to a subnet / binary notation

MAX_BITS = 32 # if subnet is /32 255.255.255.255 means all the bits are set to 1
OCTET_BITS = 8 
NUM_OCTETS = 4

def create_subnet_octets(bit_stream):
	#first determine how many octents to make
    subnet_ret = ''
    i = 0
    for char in bit_stream:
        i+=1
        if i % 8 == 0 and i is not 32:
        	subnet_ret = subnet_ret+char+'.'
        else:
            subnet_ret = subnet_ret+char
    return subnet_ret

def calculate_decimal(binary_value):
    return str(int(str(binary_value),2))

def create_subnet_decimal(bit_octet):
    subnet_ret = ''
    #first we need to check we have a valid subnet mask in binary
    if not len(bit_octet.split('.')) == 4:
        print "no valid subnet in binary"
        #os.exit(1)
    i = 0
    for octet in bit_octet.split('.'):
        i+=1
        if i == 4:
            subnet_ret = subnet_ret+calculate_decimal(octet)
            #subnet_ret = subnet_ret+str(int(str(octet)),2)
        else:
            subnet_ret = subnet_ret+calculate_decimal(octet)+'.'
    return subnet_ret
        
def cidr_to_binary(cidr):
        #first we need to calculate how many bits we have left over in an octet.
        #we do this by dividing the CIDR by 4 (amount of octets in the address) 26 / 4 = 6, means 3
    octets_filled = cidr // OCTET_BITS #this will calculate how many octets are 11111111
    octet_to_fill = cidr % OCTET_BITS # doing  cidr modules OCTET_BITS will show how many bits are supposed to be switched to 1
    bits_stream = '1'* (OCTET_BITS * octets_filled)
    bits_stream = bits_stream+'1' * octet_to_fill
    bits_stream = bits_stream + '0'* (MAX_BITS - (octet_to_fill+(octets_filled * OCTET_BITS)))
    return bits_stream

def cidr_to_decimal(cidr):
    return create_subnet_decimal(create_subnet_octets(cidr_to_binary(cidr)))