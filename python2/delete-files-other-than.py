#!/bin/env python
import time
import os
import shutil
import argparse

#This script will delete files and/or directories from a given directory on *Unix. 

#convert to str -> rsplit('.')[0]

def days_seconds(days):
    return days * 86400

parser = argparse.ArgumentParser()
parser.add_argument('-d','--days',action="store",dest="days_older",default=30,help="Will delete everything older than this value.",type=int)
parser.add_argument('-t','--type',action="store",dest="deletion_type",default="files",help="The type of file to delete. Normal(files),directories(dirs) or both(fnd).",choices=['files','dirs','fnd'],type=str)
parser.add_argument('-n','--no-dry-run',action="store_false",dest="dry_run",default=True,help="If dry-run enabled will only print file up for deletion, when false it will actually delete the files")
parser.add_argument('-f','--folders',type=str,dest='path',nargs='+',default="none")
args = parser.parse_args()

max_days_old = args.days_older# files older than this variable will be deleted
path = '.' #test variable
date_today = a = time.strftime('%Y-%m-%d', time.localtime())
epoch_today = int(time.time()) #get time in seconds 
time_grens = int((time.time() - days_seconds(max_days_old))) # argument to days second is the seconds to extract from current time to see how old files are.
data_type_to_delete = args.deletion_type #'dirs' # here we can set what kind of data we want to delete.. valid values are 'files' | 'dirs' | 'files_dirs'
dry_run = args.dry_run # if dry-run is enabled it will only print the files that are up for deletion. 

if args.path == "none":
    paths = ['/tmp']
else:
   paths = args.path

def search_directory(path_array):
    arr_files_to_return = []
    #total_in_directory = [] #returns the amount of files that are up to deletion in a certain ditectory
    if not isinstance(path_array,list):
        return "Function argument is not an array!"
    for path in path_array:
        to_delete_in_dir_files = 0
        to_delete_in_dir_dirs = 0
        if os.path.isdir(path): # check if the path supplied is indeed a directory, else no need to search through it.
            for f in os.listdir(path):
                file_date = int(os.stat(os.path.join(path,f)).st_ctime) # get ctime of file in epoch notation
                file_path = os.path.join(path,f)
                file_age = (epoch_today - file_date) / 86400 #divide age of file by 86400 seconds to get the # days
                file_type = "file" if os.path.isfile(file_path) else "directory"
                if  data_type_to_delete == 'files' and file_type == "file" and file_date < time_grens:
                    arr_files_to_return.append([file_path,file_age])
                    to_delete_in_dir_files += 1
                elif data_type_to_delete == 'dirs' and file_type == "directory" and file_date < time_grens:
                    arr_files_to_return.append([file_path,file_age])
                    to_delete_in_dir_dirs += 1                   # print 'directorie -> %s' % file_path
                elif data_type_to_delete == 'fnd' and file_type in ("file","directory") and file_date < time_grens:
                     arr_files_to_return.append([file_path,file_age])
                     if file_type == "file":
                          to_delete_in_dir_files += 1
                     else:
                           to_delete_in_dir_dirs += 1
                #elif not os.path.isdir(file_path): # we don't want to delete files, so skip them!
                #    pass
                else:
                    pass
                    #print "File %s is still within limit" % file_path
        else:
            print "Error, directory %s does not exist." % path
        if to_delete_in_dir_files != 0:
            print "Will delete %i file(s) in directory %s" % (to_delete_in_dir_files,path)
        if to_delete_in_dir_dirs !=0:
            print "Will delete %i directories in directory %s" % (to_delete_in_dir_dirs,path)
    return arr_files_to_return

def delete_files(file_array):
    deleted_files = 0
    if not isinstance(file_array,list):
        return "Function argument is not an array!"
    for file in file_array:
        deleted_files+= 1
        if not dry_run:
            if os.path.isdir(file[0]):
                print "Deleting directory %s - directory is %i day(s) old." % (file[0],file[1])
                shutil.rmtree(file[0])
            elif os.path.isfile(file[0]):
                print "Deleting file %s - file is %i day(s) old." % (file[0],file[1])
                os.remove(file[0])
            #os.remove(file)
    if dry_run:
        print "Would have deleted %i file(s). Dry run is enabled." % deleted_files
    else:
        print "Successfully deleted %i file(s)." % deleted_files

print "Will delete files that are older than %i days." % max_days_old
files_up_for_deletion = search_directory(paths)
if len(files_up_for_deletion) == 0:
    print "There are no files up for deletion."
else:
    #print "There are %i file(s) up for deletion.." % len(files_up_for_deletion)
    delete_files(files_up_for_deletion)
    #print "Do you want to delete the files?"
    #print "'y' for yes, and 'n' for no."
    #to_deleted_or_not_to = raw_input(">>> ")
    #if to_deleted_or_not_to == 'y':
    #    delete_files(files_up_for_deletion,False)
    #else:
    #    delete_files(files_up_for_deletion)