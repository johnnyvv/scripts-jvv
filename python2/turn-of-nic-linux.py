#!/bin/env python
import subprocess
import fileinput
import json
import os
import sys

def shutdown_interface():
    int_file='/etc/sysconfig/network-scripts/ifcfg-%s' % interface
    int_text='ONBOOT=yes'
    if os.path.isfile(int_file):
        f = fileinput.FileInput(int_file,inplace=True)
        for line in f:
            print line.replace(int_text,'ONBOOT=no'),
        f.close()
        subprocess.call(['ifdown', interface],shell=False)
        result['changed'] = True
    else:
        result['error'] = { 'msg': 'Failed to shutdown interface'}

result={}
json_data = json.load(sys.stdin)
interface=str(json_data['interface'])
shutdown_interface()
exitcode = 0 if 'error' not in result else 1
print json.dumps(result)
sys.exit(exitcode)